var path        = require("path")
var webpack = require('webpack')
var config = require('../webpack.config.js')
var projectsMap = require('./projects-map')

// RETRIEVE FOLDER TO WORK WITH
var folderIdx = process.argv[2]

if ( !folderIdx ) {
  console.log( 'No folder index provided, please provide one' );
  return
}

var folder = projectsMap[folderIdx]

// COMPLETE THE CONFIG 
config.entry = `./app/${folder}/index.js`
config.output.path = path.resolve(__dirname, `../public/xps/${folder}/`)

console.log('start building...');
console.log( 'input : ',path.resolve(__dirname, `../app/${folder}/index.js` ));
console.log( 'output: ', path.resolve(__dirname, `../public/xps/${folder}/main.js`) );

webpack( config, function(){
  console.log('built !');
} )