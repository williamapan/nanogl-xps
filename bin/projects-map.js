// LIST HERE ALL FOLDERS

module.exports = [
  '0-hello_nano',
  '1-texture',
  '2-fov',
  '3-phong',
  '4-picking',
  '5-assimp-model',
  '6-sombrero',
  '7-screenspace-tex',
  '8-patakk',
  '9-sphere',
  '10-pyramid-glass',
  '11-wave-pool',
  '12-light-casters'
]