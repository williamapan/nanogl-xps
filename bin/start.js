// var CMD = 'webpack-dev-server --env.dev'
var path             = require("path")
var webpack          = require('webpack')
var WebpackDevServer = require("webpack-dev-server")
var config           = require('../webpack.config.js')
var projectsMap      = require('./projects-map')

// RETRIEVE FOLDER TO WORK WITH
var folderIdx = process.argv[2]

if ( !folderIdx ) {
  console.log( 'No folder index provided, please provide one' );
  return
}

var folder = projectsMap[folderIdx]

// COMPLETE THE CONFIG 
config.entry = `./app/xps/${folder}/index.js`
config.output.path = path.resolve(__dirname, `../public/xps/${folder}/`)
config.devtool = '#inline-source-map'

// START !
var compiler = webpack(config);
var server = new WebpackDevServer(compiler, {
  contentBase: `./public/xps/${folder}/`,
  stats: { 
    colors: true,
    assets: false,
    version: false,
    hash: false,
    timings: false,
    chunks: true,
    chunkModules: false 
  }
});
server.listen(8080)