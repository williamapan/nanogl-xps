import Camera from 'nanogl-camera'
import Texture from 'nanogl/texture'

import AssetsStore from 'libs/assets-store'
import RAF from 'libs/raf'

import OrbitControls from 'camera/orbit-controls'

import StripMaterial from './materials/strip-material'
import BoxGeometry from 'geometries/box-geometry'
import PlaneBufferGeometry from 'geometries/plane-buffer-geometry'
import Mesh from 'entities/mesh'

import Grid from 'helpers/grid'
// import GUI from 'helpers/gui'

import {degreesToRadians} from 'utils/number-utils'

import Scene from './core/scene'
import Renderer from './core/renderer'
import Programs from './programs'

import Debug from './debug'

import { vec2, vec3, vec4, mat4, quat } from 'gl-matrix'

var QUAT = quat.create()

var rafId = 'raf-xp'

var gl
var elapsedTime = 0

var scene
var renderer

// camera
var camera
var controls

// to render
var grid
var plane
var cube



function init( canvas ){
  
  renderer = new Renderer( canvas )
  scene = new Scene()

  gl = renderer.gl

  initPrograms()
  initGrid()
  initPlane()
  initCube()
  initCamera()
  initControls()
  initDebug()

  // autoplay
  play()

  window.addEventListener( 'click', onClick )

}

function initPrograms( ){

  Programs.init( gl )

}

function initGrid(){

  grid = new Grid( gl )
  scene.add( grid )

  grid.rotateX( Math.PI * .5 )

}

function initPlane(){

  var prg = Programs.get( 'plane' )
  var material = new StripMaterial( gl, { prg, color: [ 1, 1, 1 ] } )
  var geometry = new PlaneBufferGeometry( gl, {
    width: 100,
    height: 100,
    widthSegments: 100,
    heightSegments: 100
  } )

  plane = new Mesh( geometry, material )
  plane.position[0] = 0
  plane.position[1] = .1
  // plane.rotateX( -Math.PI * .5 )

  scene.add( plane )

}

function initCube() {

  var prg = Programs.get( 'cube' )
  var material = new StripMaterial( gl, { prg, color: [ 1, 1, 1 ] } )
  var geometry = new BoxGeometry( gl, {
    width: 1,
    height: 1,
    widthSegments: 100,
    heightSegments: 100
  } )

  cube = new Mesh( geometry, material )
  cube.position[0] = 0
  cube.position[1] = .1
  cube.position[2] = 1
  cube.rotateX( -Math.PI * .5 )

  scene.add( cube )

  window.cube = cube

}

function initCamera(){

  camera = Camera.makePerspectiveCamera()
  camera.lens.setAutoFov( degreesToRadians(45) )
  camera.lens.near = .1
  camera.lens.far = 1000

  // camera.position[1] = 1

  scene.add( camera )

}

function initControls(){

  controls = new OrbitControls( camera, renderer.canvas )

}

function initDebug(){


}

function onClick( evt ){
  
  var r = {
    x: cube.rotation[0],
    y: 0,
    z: 0,
    w: cube.rotation[3],
    time: 0
  }

  TweenMax.to( r, 3, {
    y: Math.PI * 1.3,
    time: 50.5,
    ease: Power4.easeInOut,
    onUpdate: function(){
      // quat.set( cube.rotation, r.x, r.y, r.z, r.w );
      // cube.invalidate();
      cube.material.prg.uTime( r.time )
      quat.setAxisAngle( cube.rotation, [ 0, 1, 0 ], r.y )
      cube.invalidate()
    }
  } )

}


function render(){

  elapsedTime += RAF.dt
  
  gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT )

  scene.update()
  controls.update()

  camera.updateViewProjectionMatrix( renderer.width, renderer.height )

  // GRID
  // grid.render( camera )

  // PLANE
  plane.render( camera )

  // CUBE
  cube.render( camera )
  // cube.rotateZ( .01 )
  // cube.rotateY( .01 )
  
}

function resize( w, h ){

  renderer.resize( w, h )

}

function play(){ RAF.subscribe( rafId, render ) }
function pause(){ RAF.unsubscribe( rafId ) }

export default {
  init,
  play,
  pause,
  resize
}