attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUv;

uniform mat4 uMVP;

varying vec3 vWorldPosition;
varying vec2 vUv;



void main(){

  gl_Position = uMVP * vec4( aPosition, 1.0 );

  vWorldPosition = gl_Position.xyz / gl_Position.w;
  vUv = aUv;

}