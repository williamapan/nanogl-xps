uniform vec2 uResolution;
uniform float uTime;

varying vec3 vWorldPosition;
varying vec2 vUv;


void main(){

  vec3 color1 = vec3( 1.0, 0.0, 0.0 );
  vec3 color2 = vec3( 1.0, 1.0, 1.0 );

  float m = sin( ( gl_FragCoord.y + uTime ) / 8.0 );
  vec3 color = color1;
  if ( m < 0.0 ) color = color2;

  gl_FragColor = vec4( color, 1.0 );

}