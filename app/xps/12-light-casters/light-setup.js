import AmbientLight from 'lights/ambient-light'
import DirectionalLight from 'lights/directional-light'
import PointLight from 'lights/point-light'
import SpotLight from 'lights/spot-light'

class LightSetup {

  constructor(){

    this.ambientLights = []
    this.dirLights     = []
    this.pointLights   = []
    this.spotLights    = []

  }

  addAmbientLight( opts ) {

    var light = new AmbientLight( opts )
    this.ambientLights.push( light )

  }

  addDirectionalLight( opts ) {

    var light = new DirectionalLight( opts )
    this.dirLights.push( light )

  }

  addPointLight( opts ) {
    
    var light = new PointLight( opts )
    this.pointLights.push( light )

  }

  addSpotLight( opts ) {
    
    var light = new SpotLight( opts )
    this.spotLights.push( light )

  }

  update() {

  }

}

export default LightSetup