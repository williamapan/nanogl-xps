import Program from 'nanogl/program'
import GLUtils from 'utils/gl-utils'

var gl
var defs     = ''
var programs = {}

/**
 * Get current webgl context to play with
 * List all programs for easy readability
 * 
 * @param {WebGLRenderingContext} _gl
 */
function init( _gl ) {

  gl = _gl

  defs     = 'precision ' + GLUtils.getPrecision(gl) + ' float;\n'

  programs[ 'basic2D' ] = new Program( gl )

  _compilePrograms()

}

/**
 * Returns a program given its id
 * 
 * @param {String} programId
 */
function get( programId ){

  if ( programs[ programId ] ) return programs[ programId ]

  console.error( `[Programs get] program '${programId}' doesnt exist` )

}


/**
 * Compile all programs
 */
function _compilePrograms() {

  programs[ 'basic2D' ].compile(
    require( 'glsl/basic2D.vert' ),
    require( 'glsl/basic2D.frag' ),
    defs
  )
  
  console.log('\nprograms available : ')
  console.log( programs )
  

}


export default {
  init,
  get
}