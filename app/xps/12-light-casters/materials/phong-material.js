import { vec3, mat3, mat4 } from 'gl-matrix'
import Program from 'nanogl/program'
import GLUtils from 'utils/gl-utils'

var M4 = mat4.create();



class PhongMaterial {

  /**
   * @param {Object} opts =>
   *   @param {Texture} diffuseMap
   *   @param {Texture} specularMap
   * 
   */
  constructor( gl, opts = {} ) {
      
      this.gl          = gl
      this.prg         = new Program( gl )
      
      this.defs        = ''

      this.normalMatrix = mat3.create();

      this.diffuse     = opts.diffuse || vec3.fromValues( 1, 1, 1 )
      this.diffuseMap  = opts.diffuseMap || null  
      this.specular    = opts.specular || vec3.fromValues( 1, 1, 1 )
      this.specularMap = opts.specularMap || null  
      this.shininess   = opts.shininess || 32
      this.ambient     = this.diffuse
      
      this.dirLights = []
      this.spotLights = []

      this.compile()
      this.setUniforms()

  }

  computeDefs() {

    var gl = this.gl

    const PRECISION_DEF = `precision ${GLUtils.getPrecision(gl)} float;`

    const DIRLIGHTS_DEF = `#define NUM_DIRLIGHTS ${this.dirLights.length}`

    const DIFFUSE_MAP_DEF = ( this.diffuseMap ) 
      ? '#define DIFFUSE_MAP'
      : ''

    const SPECULAR_MAP_DEF = ( this.specularMap ) 
      ? '#define SPECULAR_MAP'
      : ''

    this.defs = `
      ${PRECISION_DEF}
      ${DIRLIGHTS_DEF}
      ${DIFFUSE_MAP_DEF}
      ${SPECULAR_MAP_DEF}
    `

  }

  compile() {

    this.computeDefs()

    this.prg.compile(
      require( '../glsl/light-casters.vert' ),
      require( '../glsl/light-casters.frag' ),
      this.defs
    )

  }

  setUniforms(){

    var prg = this.prg
    prg.use()
    
    prg[ 'uMaterial.ambient' ]( this.ambient )
    prg[ 'uMaterial.shininess' ]( this.shininess )

    if ( this.diffuseMap ) {
      prg[ 'uMaterial.diffuseMap' ]( this.diffuseMap )
    }
    if ( this.specularMap ) {
      prg[ 'uMaterial.specularMap' ]( this.specularMap )
    }

    prg[ 'uAmbientLightIntensity' ]( .15 )
    prg[ 'uAmbientLight' ]( [ 1, 1, 1 ] )
    
  }

  addLight(){

  }

  updateLights(){

  }

  prepare( node, camera ) { 

    var prg = this.prg
    prg.use()

    this.updateLights()

    if ( this.diffuseMap ) {
      prg[ 'uMaterial.diffuseMap' ]( this.diffuseMap )
    }

    if ( this.specularMap ) {
      prg[ 'uMaterial.specularMap' ]( this.specularMap )
    }
    
    camera.modelViewProjectionMatrix( M4, node._wmatrix )
    prg.uMVP( M4 )

  }

}

export default PhongMaterial