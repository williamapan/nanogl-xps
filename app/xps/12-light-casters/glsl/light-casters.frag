struct Material {
  vec3 ambient;
  vec3 diffuse;
  sampler2D diffuseMap;
  vec3 specular;
  sampler2D specularMap;
  float shininess;
};

uniform float uAmbientLightIntensity;
uniform vec3 uAmbientLight;

uniform vec3 uCameraPosition;
uniform mat3 uNormalMatrix;
uniform Material uMaterial;

varying vec2 vUv;
varying vec3 vNormal;
varying vec3 vFragPosition;

vec3 getMaterialAmbient(){

  #ifdef DIFFUSE_MAP
    vec3 ambient = texture2D( uMaterial.diffuseMap, vUv ).rgb;
  #else
    vec3 ambient = uMaterial.ambient;
  #endif

  return ambient;

}

// APPLY AMBIENT
// --------
void applyAmbient( inout vec3 color ) {

  color *= uAmbientLight * uAmbientLightIntensity;

}

void main() {
  
  vec3 color   = getMaterialAmbient();

  applyAmbient( color );

  gl_FragColor = vec4( color, 1.0 );


  // uSpecularMap;

}