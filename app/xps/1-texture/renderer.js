import GLRenderer from 'nanogl-renderer'
import Scene from './scene'

var Renderer = GLRenderer({

  init( scene ) {

    this.scene = new Scene( this )

    this.gl.enable( this.gl.DEPTH_TEST );
    this.gl.frontFace( this.gl.CCW )

  },

  getContextOptions() {

    return {
      depth:                  true,
      stencil:                true,
      antialias:              (window.devicePixelRatio<=1),
      alpha:                  false,
      premultipliedAlpha:     false,
      preserveDrawingBuffer:  false
    }
    
  },

  render( dt ) {

    // console.log( dt );
    
    this.scene.render( dt )

  },

  resize(){


  }

})

export default Renderer

