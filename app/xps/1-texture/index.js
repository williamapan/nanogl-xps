import { createCanvas } from 'utils/dom-utils'
import Renderer from './renderer'
import Scene from './scene'

var canvas
var renderer
var scene

window.addEventListener( 'DOMContentLoaded', init )

function init(){
  
  canvas = createCanvas()
  document.body.appendChild( canvas )

  renderer = new Renderer( canvas )
  renderer.play()

}
