import GLState from 'nanogl-state';
import Program from 'nanogl/program'
import Node from 'nanogl-node'
import Camera from 'nanogl-camera'
import OrbitControls from 'camera/orbit-controls'
import {degreesToRadians} from 'utils/number-utils'
import {vec3} from 'gl-matrix'

import PyramidGlass from './entities/pyramid-glass';

var prg
var camera
var elapsedTime = 0

var programs = {}
var controls

class Scene {

  constructor( renderer ){

    this.renderer = renderer
    this.gl       = renderer.gl
    this.dt       = 1/60

    this.root     = new Node()

    this.camera = null;
    this.programs = null;

    this.init()

  }

  init(){

    var gl = this.gl

    gl.state = new GLState( gl );

    this.initPrograms()
    this.initCamera()
    this.initControls()

    this.pyramidGlass = new PyramidGlass( this );
    this.root.add( this.pyramidGlass.root )

    this.renderer._checkSize()

  }

  initPrograms(){

    var gl = this.gl
    var names = [
      'basic2D',
      'basic3D',
      'pyramid-glass'
    ]

    for ( var i = 0; i < names.length; i++ ) {

      var prgName = names[i]

      var def = "precision highp float;\n"
      var prg = new Program( gl )
      prg.compile(
        require( './glsl/' +prgName+ '.vert' ),
        require( './glsl/' +prgName+ '.frag' ),
        def
      )
      prg.use()

      programs[ prgName ] = prg

    }
      
    this.programs = programs;

  }

  initCamera(){

    camera = Camera.makePerspectiveCamera()
    camera.lens.setAutoFov( degreesToRadians(45) )
    camera.lens.near = .1
    camera.lens.far = 1000

    camera.position[1] = 2

    this.root.add( camera );

    this.camera = camera;

  }

  initControls(){ 

    controls = new OrbitControls( this.renderer.canvas )
    controls.start( this.camera );
    controls.radius = 20;

  }

  render( dt ){

    var gl = this.gl

    elapsedTime += dt

    // CAMERA
    // ====================
    controls.update()
    camera.updateViewProjectionMatrix( this.renderer.width, this.renderer.height )

    this.pyramidGlass.preRender( dt );


    // UPDATE NODES'S WORLD MATRIX ( AND UPDATE CHILDREN )
    // ====================
    this.root.updateWorldMatrix()

    // RENDERING
    // ====================
    gl.bindFramebuffer(gl.FRAMEBUFFER, null)
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 )
    gl.viewport( 0, 0, this.renderer.width, this.renderer.height )
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT )

    this.pyramidGlass.render( dt );

  }

}

export default Scene