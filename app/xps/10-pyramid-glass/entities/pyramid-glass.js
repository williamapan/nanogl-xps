import glMatrix      from "gl-matrix";
import Node          from "nanogl-node";
import GLArrayBuffer from 'nanogl/arraybuffer';
import GLIndexBuffer from 'nanogl/indexbuffer';
import GUI           from '../gui';
import Textures      from '../textures';

var vec3 = glMatrix.vec3;
var mat4 = glMatrix.mat4;

var V3A  = vec3.create();
var V3B  = vec3.create();
var V3C  = vec3.create();
var M4   = mat4.create();


// ===============
// GEOMETRY
// ===============

class PyramidGlassGeom {

  constructor( gl, opts ){

    this.gl      = gl;
    this.opts    = opts;
    this.buffer  = new GLArrayBuffer( gl );
    this.buffer.attrib( "aPosition", 3,  gl.FLOAT );
    this.buffer.attrib( "aNormal", 3,  gl.FLOAT );
    this.buffer.attrib( "aBasePosition", 3,  gl.FLOAT );
    this.buffer.attrib( "aColor", 3,  gl.FLOAT );
    this.buffer.attrib( "aSpeed", 1,  gl.FLOAT );
    this.ibuffer = new GLIndexBuffer( gl, gl.UNSIGNED_SHORT );
    this.attribSize        = 0;
    for (var i = 0; i < this.buffer.attribs.length; i++) {
      this.attribSize += this.buffer.attribs[i].size;
    }

    this.particlesNumber = 2000;

  }

  // Generate geometry 
  make( ){

    var data = new Float32Array( this.particlesNumber * 4 * this.attribSize );
    var idata = [];

    var color = vec3.create();
    color[0] = 0.2;
    color[1] = 0.2;
    color[2] = 0.9;

    var c = vec3.create();

    var pos = [ 
      1,  1,  1,   
      - 1, - 1,  1,  
      - 1,  1, - 1,    
      1, - 1, - 1
    ];

    var pos = [
      0, 1, 0,
      0.4714, 0.3333, -0.8165,
      
      0.942809, -0.333333, 0,
      -0.9428, 0.3333, 0,
      
      -0.471405, -0.333333, -0.816497,
      0.4714, 0.3333, 0.8165,
      
      -0.471405, -0.333333, 0.816497,
      0, -1,  0
    ]


    var speed, scale, rx, ry, rz;
    for(var i = 0; i<this.particlesNumber*4; i+=4){

      vec3.random( V3A, Math.random() * 7 );
      vec3.random( c, Math.random()*0.1 );
      vec3.add( c, c, color );
      speed = 1   + Math.random() * 2;
      scale = 0.1 + Math.random();
      rx = Math.random() * Math.PI * 2;
      ry = Math.random() * Math.PI * 2;
      rz = Math.random() * Math.PI * 2;

      for( var j = 0; j<4; j++ ){

        V3B[ 0 ] = pos[ j*6     ] * scale;
        V3B[ 1 ] = pos[ j*6 + 1 ] * scale;
        V3B[ 2 ] = pos[ j*6 + 2 ] * scale;
        V3C[ 0 ] = pos[ j*6 + 3 ];
        V3C[ 1 ] = pos[ j*6 + 4 ];
        V3C[ 2 ] = pos[ j*6 + 5 ];


        vec3.rotateX( V3B, V3B, [0, 0, 0], rx );
        vec3.rotateY( V3B, V3B, [0, 0, 0], ry );
        vec3.rotateZ( V3B, V3B, [0, 0, 0], rz );

        vec3.rotateX( V3C, V3C, [0, 0, 0], rx );
        vec3.rotateY( V3C, V3C, [0, 0, 0], ry );
        vec3.rotateZ( V3C, V3C, [0, 0, 0], rz );

        vec3.add( V3B, V3B, V3A );

        data.set([
          // position
          V3B[ 0 ], V3B[ 1 ], V3B[ 2 ],
          // Normal
          V3C[ 0 ], V3C[ 1 ], V3C[ 2 ],
          // center
          V3A[ 0 ], V3A[ 1 ], V3A[ 2 ],
          c[   0 ], c[   1 ], c[   2 ],
          speed
        ], (i+j) * this.attribSize )

      }     

      idata.push( i     );
      idata.push( i + 1 );
      idata.push( i + 2 );

      idata.push( i     );
      idata.push( i + 2 );
      idata.push( i + 3 );

      idata.push( i + 1 );
      idata.push( i + 2 );
      idata.push( i + 3 );

      idata.push( i + 1 );
      idata.push( i + 2 );
      idata.push( i + 3 );

    }

    this.buffer.data( data );

    this.ibuffer.data( new Uint16Array( idata ) );

  }

  setup( prg ){

    this.buffer.attribPointer( prg );
    this.ibuffer.bind();

  }

  render(){

    this.ibuffer.drawTriangles();

  }

}








// ===============
// PARTICLES MAIN
// ===============

class PyramidGlass {

  constructor( scene ){

    var gl = scene.gl;

    this.gl = gl;

    // Default
    this.scene      = scene;
    this._loadables = [];
    this.root       = new Node();

    this.prg        = scene.programs["pyramid-glass"];

    // Uniforms
    this.time      = 0;
    this.scale     = 1;

    // Config
    this.glCfg = gl.state.config();
    this.glCfg
      .enableDepthTest( true )
      .depthMask( true );
      // .enableBlend()
      // .blendFunc(gl.ONE, gl.ONE);

    this.geom = new PyramidGlassGeom( this.gl );
    this.geom.make( );

    GUI.add( this, 'scale', 0, 10 ).listen();

    document.addEventListener( "click", ()=>{
      // this.scale += 0.5;
    });

  }


  preRender( dt ) {

    this.time += dt;
    // this.scale -= dt;
    this.scale = Math.max( this.scale, 0 );

  }

  render( ){

    if(!this.geom){ return }

    var prg    = this.prg,
        geom   = this.geom,
        camera = this.scene.camera;


    prg.use()

    camera.modelViewProjectionMatrix( M4, this.root._wmatrix );
    prg.uMVP( M4 );
    prg.uTime( this.time );
    prg.uScale( this.scale );

    prg.tCube( Textures.getTexture( 'env' ) );

    geom.setup( prg );

    this.glCfg.apply();
    geom.render();

  }

}




export default PyramidGlass;