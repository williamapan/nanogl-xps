import { createCanvas } from 'utils/dom-utils'
import Renderer from './renderer'
import Scene from './scene'
import Textures from './textures';

var canvas
var renderer
var scene

window.addEventListener( 'DOMContentLoaded', init )

function init(){
  
  canvas = createCanvas()
  document.body.appendChild( canvas )


  renderer = new Renderer( canvas )
  Textures.setup( renderer.gl );
  Textures.load( ).then( ()=>{
    renderer.play()
  });


}
