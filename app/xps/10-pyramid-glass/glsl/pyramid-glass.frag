uniform float uTime;
uniform samplerCube tCube;

varying vec3 vPosition;
varying vec3 vColor;
varying vec3 vNormal;

void main(){

  vec3 dir = vNormal;
  dir.y = -dir.y;
  
  vec4 color = textureCube( tCube, dir );
  // attribute color
  // vec3 color = vColor;

  gl_FragColor = vec4( color ); 

}