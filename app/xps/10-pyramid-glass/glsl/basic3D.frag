uniform float uTime;

varying vec3 vPosition;
varying vec3 vColor;
varying vec2 vUv;

void main(){

  float toto = uTime;

  // position color
  vec3 color = vec3( vPosition.x * 0.5, vPosition.y * 0.5 + 0.5, vPosition.z );
  
  // attribute color
  // color = vColor;

  // texture color
  // color = texture2D( uTexture, vUv ).rgb;

  gl_FragColor = vec4( color, 1.0 ); 

}