attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec3 aBasePosition;
attribute vec3 aColor;
attribute float aSpeed;

uniform float uTime;
uniform mat4 uProjectionMatrix;
uniform mat4 uMVP;
uniform mat4 uWorldMatrix;
uniform mat4 uModelMatrix;
uniform float uScale;

varying vec3 vPosition;
varying vec3 vColor;
varying vec3 vNormal;

#pragma glslify: cnoise     = require( ./includes/curl-noise )

void main(){

  vec3 p = mix( aPosition, aBasePosition, abs( sin( uTime * aSpeed + aSpeed ) ) );
  vec3 n = cnoise( (p*0.1) + uTime * 0.15 );// * (2.0 * abs( sin( uTime * aSpeed + aPosition.x ) ) );
  p += n * uScale;
  // p = mix( aPosition, aBasePosition, abs( sin( uTime * aSpeed ) ) );
  gl_Position = uMVP * vec4( p,  1.0 );

  gl_PointSize = clamp( ( 10.0 ) * ( 1.0 - length( aPosition )/5.0 ), 0.0, 10.0 );
  vPosition = aPosition; 
  vColor = aColor * length( n );
  vNormal = normalize( aNormal + n * uScale );

}