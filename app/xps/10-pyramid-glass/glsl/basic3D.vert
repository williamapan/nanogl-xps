attribute vec3 aPosition;
attribute vec3 aColor;
attribute vec2 aUv;

uniform mat4 uProjectionMatrix;
uniform mat4 uMVP;
uniform mat4 uWorldMatrix;
uniform mat4 uModelMatrix;

varying vec3 vPosition;
varying vec3 vColor;
varying vec2 vUv;

void main(){
  // uM
  // uWorldMatrix
  // uMVP
  // gl_Position = uMVP * vec4( aPosition.xy, 1.0,  1.0 );

  gl_Position = uMVP * vec4( aPosition,  1.0 );

  vPosition = aPosition; 
  vColor = aColor;
  vUv = aUv;

}