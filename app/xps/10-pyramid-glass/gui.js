
var Dat = require( 'dat-gui' )

var DISABLED = false;
var CLOSED   = false;

var gui = new Dat.GUI()

if( DISABLED ){
  gui.domElement.parentNode.removeChild(gui.domElement);
}

if( CLOSED ){
  gui.close();
}

module.exports = gui;
