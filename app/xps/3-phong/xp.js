import Camera from 'nanogl-camera'
import Texture from 'nanogl/texture'

import AssetsStore from 'libs/assets-store'
import RAF from 'libs/raf'

import OrbitControls from 'camera/orbit-controls'

import BoxGeometry from 'geometries/box-geometry'
import BasicMaterial from 'materials/basic-material'
import PhongMaterial from 'materials/phong-material'

import Mesh from 'entities/mesh'

import Grid from 'helpers/grid'
// import GUI from 'helpers/gui'

import Light from 'lights/light'

import {degreesToRadians} from 'utils/number-utils'

import Scene from './scene'
import Renderer from './renderer'
import Programs from './programs'

// import Debug from './debug'


var rafId = 'raf-xp'

var gl
var elapsedTime = 0

var scene
var renderer

// camera
var camera
var controls

// to render
var grid
var cube
var light

// debug
var materialOpts = {
  ambient: [ 255, 128, 79 ]
}



function init( canvas ){
  
  renderer = new Renderer( canvas )
  scene = new Scene()

  gl = renderer.gl

  initPrograms()
  initGrid()
  initLight()
  initCube()
  initCamera()
  initControls()
  // initDebug()

  // autoplay
  play()

}

function initPrograms( ){

  Programs.init( gl )

}

function initCube() {

  var diffuseImg = AssetsStore.get( 'diffuse.jpg' )
  var diffuseMap = new Texture( gl )
  diffuseMap.fromImage( diffuseImg )

  var specularImg = AssetsStore.get( 'specular.jpg' )
  var specularMap = new Texture( gl )
  specularMap.fromImage( specularImg )

  var prg = Programs.get( 'phongMaterial' )
  var geometry = new BoxGeometry( gl )
  var material = new PhongMaterial( gl, {
    prg,
    diffuse: [ 1.0, 0.5, 0.31 ],
    diffuseMap,
    specularMap
  } )

  cube = new Mesh( geometry, material )
  cube.position[1] = .5

  cube.material.addLight( light )

  scene.add( cube )

  
}

function initLight(){

  var prg = Programs.get( 'basicMaterial' )
  var geometry = new BoxGeometry( gl )
  var material = new BasicMaterial( gl, { prg } )
  var mesh = new Mesh( geometry, material )

  light = new Light({ mesh })

  light.position = [ 0, 1.5, 0 ]
  light.mesh.setScale( .05 )

  scene.add( light.mesh )

}

function initGrid(){

  grid = new Grid( gl )
  scene.add( grid )

  grid.rotateX( Math.PI * .5 )

}

function initCamera(){

  camera = Camera.makePerspectiveCamera()
  camera.lens.setAutoFov( degreesToRadians(45) )
  camera.lens.near = .1
  camera.lens.far = 1000

  camera.position[1] = 2

  scene.add( camera )

}

function initControls(){

  controls = new OrbitControls( renderer.canvas )
  controls.start( camera )

}

function initDebug(){

  Debug.init()
  Debug.setMesh( cube )
  Debug.setLight( light )

}

function render(){

  elapsedTime += RAF.dt
  
  gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT )

  scene.update()
  controls.update()

  camera.updateViewProjectionMatrix( renderer.width, renderer.height )

  // GRID
  // grid.render( camera )

  // LIGHT MESH
  if ( light.auto ) {

    var radius = 2
    var t = elapsedTime * 0.0005
    var x = Math.sin( t ) * radius
    var y = 0.5
    var z = Math.cos( t ) * radius
    light.position = [ x, y, z ]

  }
  
  // light.mesh.render( camera )

  // CUBE
  cube.render( camera )
  // cube.scale[0] =  Math.abs( Math.sin( elapsedTime * 0.002 ) )
  // cube.invalidate()
}

function resize( w, h ){

  renderer.resize( w, h )

}

function play(){ RAF.subscribe( rafId, render ) }
function pause(){ RAF.unsubscribe( rafId ) }

export default {
  init,
  play,
  pause,
  resize
}