import GUI from 'helpers/gui'
import {
  hex2rgb,
  normalizeRGB
} from 'utils/number-utils'

var meshOpts = {
  geometry: {
    position: { x: 0.001, y: 0.501, z: 0.001 },
    rotation: { x: 0.001, y: 0.001, z: 0.001 },
    scale:    { x: 0.001, y: 0.001, z: 0.001 }
  },
  material: {
    ambient:   [ 255, 128, 79 ],
    diffuse:   [ 255, 128, 79 ],
    specular:  [ 255, 128, 79 ],
    shininess: 32
  }
}

var lightOpts = {
  intensity: 1,
  diffuse:   [ 255, 255, 255 ],
  specular:  [ 255, 255, 255 ],
  position:  { x: 0, y: 3, z: 0 }
}

var mesh
var light

function init(){

  initMeshDebug()
  

}

function initMeshDebug(){

  var meshFolder = GUI.addFolder('Mesh')

  var geometryFolder = meshFolder.addFolder( 'geometry' )
  geometryFolder.add( meshOpts.geometry.position, 'x', -10, 10 ).name('position x').onChange( update )
  geometryFolder.add( meshOpts.geometry.position, 'y', -10, 10 ).name('position y').onChange( update )
  geometryFolder.add( meshOpts.geometry.position, 'z', -10, 10 ).name('position z').onChange( update )

  var materialFolder = meshFolder.addFolder('material')
  materialFolder.addColor( meshOpts.material, 'ambient' ).onChange( update )
  materialFolder.addColor( meshOpts.material, 'diffuse' ).onChange( update )
  materialFolder.addColor( meshOpts.material, 'specular' ).onChange( update )
  materialFolder.add( meshOpts.material, 'shininess', 0, 512 ).onChange( update )


  function update() {

    var position  = meshOpts.geometry.position
    var rotation  = meshOpts.geometry.rotation
    var scale     = meshOpts.geometry.scale

    mesh.position[0] = position.x
    mesh.position[1] = position.y
    mesh.position[2] = position.z
    mesh.invalidate()

    var prg       = mesh.material.prg
    var ambient   = normalizeRGB( meshOpts.material.ambient )
    var diffuse   = normalizeRGB( meshOpts.material.diffuse )
    var specular  = normalizeRGB( meshOpts.material.specular )
    var shininess = meshOpts.material.shininess

    prg.use()
    prg[ 'uMaterial.ambient' ]( ambient )
    prg[ 'uMaterial.diffuse' ]( diffuse )
    prg[ 'uMaterial.specular' ]( specular )
    prg[ 'uMaterial.shininess' ]( shininess )

  }

}

function initLightDebug(){

  // fork it a bit to be able to debug
  light.position.x = light.position[0]
  light.position.y = light.position[1]
  light.position.z = light.position[2]
  light.auto = false

  var lightFolder = GUI.addFolder('Light')

  lightFolder.add( light, 'intensity', 0, 1 ).onChange( update )
  lightFolder.addColor( light, 'diffuse' ).onChange( update )
  lightFolder.addColor( light, 'specular' ).onChange( update )

  var positionFolder = lightFolder.addFolder('position')
  positionFolder.add( light.position, 'x', -10, 10 ).name('x').onChange( update )
  positionFolder.add( light.position, 'y', -10, 10 ).name('y').onChange( update )
  positionFolder.add( light.position, 'z', -10, 10 ).name('z').onChange( update )
  positionFolder.add( light, 'auto' )

  function update(){

    var prg       = mesh.material.prg
    var intensity = light.intensity
    var diffuse   = normalizeRGB( light.diffuse )
    var specular  = normalizeRGB( light.specular )
    var position  = [ light.position.x, light.position.y, light.position.z ]

    prg.use()
    prg[ 'uLight.intensity' ]( intensity )
    prg[ 'uLight.diffuse' ]( diffuse )
    prg[ 'uLight.specular' ]( specular )

    light.position = position

  }

}

function setMesh( _mesh ) {

  mesh = _mesh

}

function setLight( _light ) {

  light = _light

  initLightDebug()

}

export default {
  init,
  setMesh,
  setLight,
  meshOpts,
  lightOpts
}