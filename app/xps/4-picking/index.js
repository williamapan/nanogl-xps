import { createCanvas } from 'utils/dom-utils'
import AssetsStore from 'libs/assets-store'
import XP from './xp'

import manifest from './manifest'


window.addEventListener( 'DOMContentLoaded', init )
window.addEventListener( 'resize', resize )

function load(){

  AssetsStore.onLoadProgress = function(p){
    console.log( 'load progress - ', p)
  }

  AssetsStore.onLoadComplete = function(){
    console.log( 'load complete - init' )
    init()
  }

  AssetsStore.loadManifest( manifest )

}

function init(){
  
  var canvas = createCanvas()
  document.body.appendChild( canvas )

  XP.init( canvas )

}

function resize(){

  var w = window.innerWidth
  var h = window.innerHeight

  XP.resize( w, h ) 

}
