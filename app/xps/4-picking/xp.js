import Camera from 'nanogl-camera'
import Texture from 'nanogl/texture'

import AssetsStore from 'libs/assets-store'
import RAF from 'libs/raf'

import OrbitControls from 'camera/orbit-controls'

import BoxGeometry from 'geometries/box-geometry'
import BasicMaterial from 'materials/basic-material'

import Mesh from 'entities/mesh'

import Grid from 'helpers/grid'
import GUI from 'helpers/gui'

import Light from 'lights/light'

import {degreesToRadians} from 'utils/number-utils'

import Scene from './scene'
import Renderer from './renderer'
import Programs from './programs'

import Debug from './debug'

import { vec2, vec3, vec4, mat4 } from 'gl-matrix'


var rafId = 'raf-xp'

var gl
var elapsedTime = 0

var scene
var renderer

// camera
var camera
var controls

// to render
var grid
var plane

// raycasting
var mouse = vec2.create()
var ray_nds = vec3.create()
var ray_clip = vec4.create()
var ray_eye = vec3.create()
var ray_world = vec3.create()
var inverseProjectionMatrix = mat4.create()
var inverseViewMatrix = mat4.create()

var isIntersecting = false


function init( canvas ){
  
  renderer = new Renderer( canvas )
  scene = new Scene()

  gl = renderer.gl

  initPrograms()
  initGrid()
  initPlane()
  initCamera()
  initControls()
  initDebug()

  // autoplay
  play()

  window.addEventListener( 'mousemove', onMouseMove )

}

function initPrograms( ){

  Programs.init( gl )

}

function initGrid(){

  grid = new Grid( gl )
  scene.add( grid )

  grid.rotateX( Math.PI * .5 )

}

function initPlane(){

  var prg = Programs.get( 'basicMaterial' )
  var material = new BasicMaterial( gl, { diffuseColor: [ 1, 1, 1 ] } )
  var geometry = new BoxGeometry( gl )

  plane = new Mesh( geometry, material )
  plane.position[0] = .5
  plane.position[1] = .5

  scene.add( plane )

}

function initCamera(){

  camera = Camera.makePerspectiveCamera()
  camera.lens.setAutoFov( degreesToRadians(45) )
  camera.lens.near = .1
  camera.lens.far = 1000

  camera.position[1] = 2

  scene.add( camera )

}

function initControls(){

  controls = new OrbitControls( camera, renderer.canvas )

}

function initDebug(){


}

function onMouseMove( evt ){

  mouse[0] = evt.pageX
  mouse[1] = evt.pageY

}

function raycast(){

  var screenWidth  = renderer.width
  var screenHeight = renderer.height

  // Normalised coordinates
  ray_nds[0] = ( mouse[0] / screenWidth  ) * 2 - 1
  ray_nds[1] = ( mouse[1] / screenHeight  ) * -2 + 1
  ray_nds[2] = 1

  // Homogeneous clip coordinates
  ray_clip = vec4.fromValues( ray_nds[0], ray_nds[1], -1, 1 )

  // Eye coordinates
  var projectionMatrix = camera.lens.getProjection()
  mat4.invert(  inverseProjectionMatrix, projectionMatrix  )
  mat4.multiply( ray_eye, inverseProjectionMatrix, ray_clip )
  var v4_ray_eye = vec4.fromValues( ray_eye[0], ray_eye[1], -1, 0 )

  // world coordinates
  var projectionMatrix = camera._view
  mat4.invert(  inverseViewMatrix, projectionMatrix  )
  mat4.multiply( ray_world, inverseViewMatrix, v4_ray_eye )
  vec3.normalize( ray_world, ray_world )

}

function render(){

  elapsedTime += RAF.dt

  raycast()
  
  gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT )

  scene.update()
  controls.update()

  camera.updateViewProjectionMatrix( renderer.width, renderer.height )

  // GRID
  grid.render( camera )

  // PLANE
  plane.render( camera )
  
}

function resize( w, h ){

  renderer.resize( w, h )

}

function play(){ RAF.subscribe( rafId, render ) }
function pause(){ RAF.unsubscribe( rafId ) }

export default {
  init,
  play,
  pause,
  resize
}