uniform vec2 uResolution;
uniform vec3 uColor;
uniform float uTime;

varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;

void main(){

  // vec3 position = vPosition / 10.0;

  vec3 color = vec3( ( mod( vPosition.y * 2.0 + uTime * 0.00006, 1.0 ) * 10.0 ) );

  if ( fract( color.r ) <= 0.4 ) {
    color = vec3( 1.0 );
  }
  else {
    color = vec3( 0.0 );
  }

  gl_FragColor = vec4( color, 1.0 );

  uTime;
  uColor;

}