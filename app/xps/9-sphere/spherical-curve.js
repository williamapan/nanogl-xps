import SpericalCurveGeometry from './geometries/spherical-curve-geometry'
import SpericalCurveMaterial from './materials/spherical-curve-material'
import Mesh from 'entities/mesh'


class SphericalCurve extends Mesh {

  constructor( gl, o) {
    super()

    this.geometry = new SpericalCurveGeometry( gl, o.geometry )
    this.material = new SpericalCurveMaterial( gl, o.material )

    this.speed      = o.lineSpeed || 1
    this.lineLength = o.lineLength || 15
    this.offsetMax  = this.geometry.indices.length
    this.offset     = Math.floor(Math.random() * this.offsetMax)
    this.count      = o.lineCount || 5

  }

  render( camera ){

    this.material.prepare( this, camera )
    this.geometry.bind( this.material.prg )

    // Render lines
    this.offset += this.speed
    this.offset %= this.offsetMax

    for (let i = 0, offset, progress; i < this.count; i++) {
      progress = (this.offsetMax / this.count)

      offset  = this.offset + progress * i
      offset %= this.offsetMax

      this.material.offset[0] = offset / this.offsetMax
      this.material.offset[1] = this.lineLength / this.offsetMax

      if (this.material.prg.uOffset) this.material.prg.uOffset( this.material.offset )
      this.geometry.drawRange( offset, offset + this.lineLength )
    }

  }

}

module.exports = SphericalCurve