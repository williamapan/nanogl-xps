attribute vec3 position;
attribute float progress;

uniform mat4 uMVP;

varying float vProgress;

void main(){
  vProgress = progress;
  gl_Position = uMVP * vec4( position, 1.0 );
}