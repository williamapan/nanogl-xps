uniform vec3 uColor;
uniform float uOffset[2];

varying float vProgress;

void main(){

  float offset = uOffset[0];
  float len    = uOffset[1];

  float alpha  = (vProgress - offset) / len;
  gl_FragColor = vec4( uColor * alpha, alpha );
}