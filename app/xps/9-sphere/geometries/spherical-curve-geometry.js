'use strict'

import Geometry from 'geometries/geometry'
import BezierEasing from '../vendor/bezier-easing'

const easing = BezierEasing( 0.34, 0.88, 0.83, 0.11 )

const PI  = Math.PI
const PI2 = PI * 2

const _clamp = function( value, min, max ) {
  return Math.max(min, Math.min( value, max ))
}

const _generate = function( o ) {

  let count      = o.count || 100
  let startAngle = o.startAngle || 0
  let progress   = 0

  const vertices = []
  const indices  = []

  const radius = o.radius || 1

  const gapSize = o.gapSize || 0.05

  let curvature = 0,
  attribCount   = 4,
  ANGLE         = 0,
  progressY     = 0,
  LAPS          = PI2 * 2.5

  for (let i = 0, j = 0; i < count; i++, j+=attribCount) {

    progress = i / count

    ANGLE = easing(progress) * LAPS

    curvature = gapSize + Math.sin( progress * PI )

    vertices[j+0] = Math.cos( ANGLE + startAngle ) * (curvature * radius)
    vertices[j+1] = Math.cos( progress * PI ) * (1. + gapSize * 0.75) * radius
    vertices[j+2] = Math.sin( ANGLE + startAngle ) * (curvature * radius)

    vertices[j+3] = progress

    indices.push( i )

  }

  return { vertices, indices }

}


// Firefox
  // var x = Math.cos( progress * PI2 * 2 )
  // var z = Math.sin( progress * PI2 * 2 )


  // curvature = Math.sin( progress * PI )

  // vertices[j+0] = x + Math.cos( progress * PI2 + startAngle ) * (curvature * radius)
  // vertices[j+1] = Math.cos( progress * PI ) * radius
  // vertices[j+2] = z + Math.sin( progress * PI2 + startAngle ) * (curvature * radius)





class SphericalCurveGeometry extends Geometry {

  constructor( gl, o ) {
    const { vertices, indices } = _generate( o || {} )
    super( gl, vertices, indices )

    // this.drawingMethod = 'drawLines'
    this.drawingMethod = 'drawLineStrip'
  }

  allocate(){

    super.allocate()

    var gl = this.gl

    this.attrib( 'position', 3, gl.FLOAT )
    this.attrib( 'progress', 1, gl.FLOAT )

  }

  drawRange( start, end ) {
    if (this.indices) {
      this.drawElementRange( start, end )
    } else {
      this.drawArrayRange( start, end )
    }
  }

  drawElementRange( start, end ) {
    const len = this.drawingBuffer.size / this.drawingBuffer.typeSize

    let offset = _clamp(start, 0, len)
    let count  = Math.max( _clamp(end, 0, len) - offset, 0 )

    this.drawingBuffer[ this.drawingMethod ]( count, offset * this.drawingBuffer.typeSize )
  }

  drawArrayRange( start, end ) {
    const len = this.drawingBuffer.length

    let offset = _clamp(start, 0, len)
    let count  = Math.max( _clamp(end, 0, len) - offset, 0 )

    this.drawingBuffer[ this.drawingMethod ]( count, offset )
  }

}

module.exports = SphericalCurveGeometry