import Camera from 'nanogl-camera'
import Texture from 'nanogl/texture'
import PostProcess from './post-process'

import AssetsStore from 'libs/assets-store'
import RAF from 'libs/raf'

import OrbitControls from 'camera/orbit-controls'

import PlaneGeometry from 'geometries/plane-geometry'
// import PlaneBufferGeometry from 'geometries/plane-buffer-geometry'

import Sphere from './sphere'

import Mesh from 'entities/mesh'

import Grid from 'helpers/grid'
// import GUI from 'helpers/gui'

import {degreesToRadians} from 'utils/number-utils'

import Scene from './core/scene'
import Renderer from './core/renderer'
import Programs from './programs'

import GLState from 'nanogl-state'

import Debug from './debug'

import { vec2, vec3, vec4, mat4 } from 'gl-matrix'


var rafId = 'raf-xp'

var gl
var elapsedTime = 0

var scene
var renderer

// camera
var camera
var controls

// to render
var grid
var plane
let sphere

// Post Process
var postProcess
var post



function init( canvas ){

  renderer = new Renderer( canvas )
  scene = new Scene()

  gl = renderer.gl
  gl.state = new GLState( gl )

  postProcess = new PostProcess( gl )

  initPrograms()
  // initGrid()
  initCamera()
  initControls()
  initDebug()

  initSphere()

  // autoplay
  play()

  // window.addEventListener( 'mousemove', onMouseMove )

}

function initPrograms( ){

  Programs.init( gl )

}

function initSphere() {

  sphere = new Sphere( gl, { count: 200, sphereCount: 1, radius: 1, gapSize: 0.25 } )
  scene.add( sphere )

}

function initGrid(){

  grid = new Grid( gl )
  scene.add( grid )

  grid.rotateX( Math.PI * .5 )

}

function initCamera(){

  camera = Camera.makePerspectiveCamera()
  camera.lens.setAutoFov( degreesToRadians(45) )
  camera.lens.near = .1
  camera.lens.far = 1000

  camera.position[1] = 2

  scene.add( camera )

}

function initControls(){

  controls     = new OrbitControls( renderer.canvas )
  controls.phi = Math.PI * 0.5
  controls.start( camera )

}

function initDebug(){


}

function onMouseMove( evt ){

  mouse[0] = evt.pageX
  mouse[1] = evt.pageY

}


function render(){

  elapsedTime += RAF.dt

  // gl.bindFramebuffer(gl.FRAMEBUFFER, null);
  // gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT )

  scene.update()
  controls.update()

  camera.updateViewProjectionMatrix( renderer.width, renderer.height )

  postProcess.post.preRender( renderer.width, renderer.height )
  // GRID
  if (!!grid) grid.render( camera )

  sphere.render( camera )

  postProcess.post.render()

  gl.state.apply()

}

function resize( w, h ){

  renderer.resize( w, h )

}

function play(){ RAF.subscribe( rafId, render ) }
function pause(){ RAF.unsubscribe( rafId ) }

export default {
  init,
  play,
  pause,
  resize
}