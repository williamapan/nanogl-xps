'use strict'

// const SphericalCurveGeometry = require('./geometries/spherical-curve-geometry')
// const SphericalCurveMaterial = require('./materials/spherical-curve-material')
// const Mesh                   = require('entities/mesh').default
const SphericalCurve = require('./spherical-curve')
const Node           = require('nanogl-node')
const Programs       = require('./programs').default

const PI   = Math.PI
const PI2  = PI * 2

class Sphere extends Node {

  constructor( gl, o ) {
    super()

    this.gl = gl

    this.cfg = gl.state.config()
    this.cfg.enableBlend()
    this.cfg.blendFunc( gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA )
    this.cfg.lineWidth( 2 )
    // this.cfg.enableDepthTest( false )

    o = o || {}
    this._generate( o )
  }

  _generate( o ) {

    const gl = this.gl

    const count = o.sphereCount || 20

    const prg = Programs.get('spherical-curve')

    let m, g, progress

    for (let i = 0; i < count; i++) {
      progress = i / count

      const sc = new SphericalCurve( gl, {
        material: { prg, color: [ 1,1,1 ] },
        geometry: Object.assign({ startAngle: PI2 * Math.random() * performance.now() }, o)
      })

      this.add( sc )
    }

  }

  render( camera ) {

    this.gl.state.apply( this.cfg )

    for (let j = 0, len = this._children.length; j < len; j++) {
      this._children[j].render( camera )
    }

    this.rotateY( -0.0025 )

  }

}

module.exports = Sphere