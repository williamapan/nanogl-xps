import Post from './vendor/nanogl-post/post'
import Bloom from './vendor/nanogl-post/effects/bloom'
import SimpleBlur from './vendor/nanogl-post/effects/simple-blur'
import Fetch from './vendor/nanogl-post/effects/fetch'

const GUI = require('dat-gui').GUI

class PostProcess {

  constructor( gl ) {
    this.gl   = gl
    this.post = new Post( gl )



    this.GUI = new GUI




    this.bloomPrm = {
      color: [ 1.8, 1.8, 1.8 ],
      size: 0.05
    }

    this.blurPrm = {
      size: 0.0035,
      intensity: 0.02
    }

    this.blur  = new SimpleBlur( this.blurPrm.size, this.blurPrm.intensity )
    this.bloom = new Bloom( this.bloomPrm.color, this.bloomPrm.size )

    this.post.add( new Fetch )
    this.post.add( this.blur )
    // this.post.add( this.bloom )

    this.GUI.add(this.blur, 'size', 0, 5)
    this.GUI.add(this.blur, 'intensity', 0, 5)

    this.GUI.add(this.bloom, 'size', 0, 5).name('bloom_size')
  }

}

module.exports = PostProcess