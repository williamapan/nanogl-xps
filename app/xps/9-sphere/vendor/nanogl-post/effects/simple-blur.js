
var Texture    = require( 'nanogl/texture' );
var BaseEffect = require( 'nanogl-post/effects/base-effect' );
var Program    = require( 'nanogl/program' );
var Fbo        = require( 'nanogl/fbo' );

var prc_frag = require( '../glsl/templates/simple-blur_process.frag' );
var prc_vert = require( '../glsl/templates/main.vert.js' )();

var TEX_SIZE = 2048;
// var TEX_SIZE = 1024;

function SimpleBlur( size, intensity ){
  BaseEffect.call( this );

  this.size      = size;
  this.intensity = intensity;

  this._preCode = require( '../glsl/templates/simple-blur_pre.frag.js' )();
  this._code    = require( '../glsl/templates/simple-blur.frag.js' )();

}


SimpleBlur.prototype = Object.create( BaseEffect.prototype );
SimpleBlur.prototype.constructor = SimpleBlur;


SimpleBlur.prototype.init = function( precode, code ){

  var gl = this.post.gl;

  var defs = '\n';
  defs += 'precision highp float;\n';

  this.prcPrg = new Program( gl );
  this.prcPrg.compile( prc_vert, prc_frag , defs );

  this.target = new Fbo( gl, {
    type    : gl.UNSIGNED_BYTE,
    format  : gl.RGB
  });
  this.target.color.clamp();
  this.target.resize( TEX_SIZE, TEX_SIZE );

}

SimpleBlur.prototype.genCode = function( precode, code ) {
  precode.push( this._preCode )
  code.   push( this._code )
}

SimpleBlur.prototype.preRender = function() {

  this.target.bind();
  this.target.clear();
  this.prcPrg.use();
  this.prcPrg.tInput( this.post.mainFbo.color );
  this.prcPrg.uSize( this.size );
  this.prcPrg.uIntensity( this.intensity );
  this.post.fillScreen( this.prcPrg, true );
  this.post.lastFBO = this.target;

}

SimpleBlur.prototype.setupProgram = function( prg ) {

  prg.tSimpleBlur( this.target.color );

}

module.exports = SimpleBlur;
