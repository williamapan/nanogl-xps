uniform sampler2D tInput;

uniform float uSize;
uniform float uIntensity;

varying vec2 vTexCoordVP;

void main(void)
{

  vec3 color = vec3(0.0);

  vec2 s = vTexCoordVP;
  color  = texture2D( tInput, s).rgb;

  vec3 c = vec3( 0.0 );
  c += texture2D( tInput, vec2( s.x + uSize, s.y         )).rgb;
  c += texture2D( tInput, vec2( s.x - uSize, s.y         )).rgb;
  c += texture2D( tInput, vec2( s.x        , s.y + uSize )).rgb;
  c += texture2D( tInput, vec2( s.x        , s.y - uSize )).rgb;
  c += texture2D( tInput, vec2( s.x + uSize, s.y + uSize )).rgb;
  c += texture2D( tInput, vec2( s.x - uSize, s.y - uSize )).rgb;
  c += texture2D( tInput, vec2( s.x - uSize, s.y + uSize )).rgb;
  c += texture2D( tInput, vec2( s.x + uSize, s.y - uSize )).rgb;

  color += c * uIntensity;

  gl_FragColor = vec4( color, 0.0 );

}
