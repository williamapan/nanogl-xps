attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUv;

uniform mat4 uMVP;

varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;
varying float vDist;

void main(){

  vec2 center = vec2( 0.5, 0.5 );
  float dist  = length( aUv -  center );
  float z     = 1.0 - dist;
  vec3 newPos = vec3( aPosition.x, aPosition.y, -pow(z, 16.0) );

  gl_Position = uMVP * vec4( newPos, 1.0 );

  vPosition = aPosition;
  vNormal = aNormal;
  vUv = aUv;
  vDist = dist;

}