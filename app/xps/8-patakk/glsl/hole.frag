uniform vec2 uResolution;
uniform float uTime;

varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;
varying float vDist;

void main(){

  // strip colors
  vec3 color1 = vec3( 0.298039216, 0.137254902, 0.462745098 );
  vec3 color2 = vec3( 0.91372549, 0.2, 0.02745098 );

  float sinus = sin( ( vUv.y + uTime * 0.00002 ) * 300.0 );
  sinus = sinus * step( .7, sinus );
  
  // darken center
  float shadow = 1.0;
  float threshold = .25;
  shadow = clamp( vDist/threshold + .55, 0.15, 1.0);

  vec3 finalColor = mix( color2, color1, sinus ) * shadow;

  gl_FragColor = vec4( finalColor, 1.0 );

  uTime;

}