import Camera from 'nanogl-camera'
import Texture from 'nanogl/texture'

import AssetsStore from 'libs/assets-store'
import RAF from 'libs/raf'

import OrbitControls from 'camera/orbit-controls'
import MaxControls from 'camera/max-controls'

import HoleMaterial from './materials/hole-material'
import PlaneGeometry from 'geometries/plane-geometry'
import Mesh from 'entities/mesh'

import Grid from 'helpers/grid'
import GUI from 'helpers/gui'

import {degreesToRadians} from 'utils/number-utils'

import Scene from './core/scene'
import Renderer from './core/renderer'
import Programs from './programs'

import Debug from './debug'

import { vec2, vec3, vec4, mat4 } from 'gl-matrix'


var rafId = 'raf-xp'

var gl
var elapsedTime = 0

var scene
var renderer

// camera
var camera
var controls
var orbitControls
var maxControls

// to render
var grid
var plane



function init( canvas ){
  
  renderer = new Renderer( canvas )
  scene = new Scene()

  gl = renderer.gl

  initPrograms()
  initGrid()
  initPlane()
  initCamera()
  initControls()
  initDebug()

  // autoplay
  play()

  // window.addEventListener( 'mousemove', onMouseMove )

}

function initPrograms( ){

  Programs.init( gl )

}

function initGrid(){

  grid = new Grid( gl )
  scene.add( grid )

  grid.rotateX( Math.PI * .5 )

}

function initPlane(){

  var prg = Programs.get( 'hole' )
  var material = new HoleMaterial( gl, { prg, color: [ 1, 1, 1 ] } )
  var geometry = new PlaneGeometry( gl, {
    width: 4,
    height: 4,
    widthSegments: 200,
    heightSegments: 200
  } )

  plane = new Mesh( geometry, material )
  plane.position[0] = 0
  plane.position[1] = 0
  plane.rotateX( -Math.PI * .5 )
  plane.setScale( 10 )

  scene.add( plane )

}


function initCamera(){

  camera = Camera.makePerspectiveCamera()
  camera.lens.setAutoFov( degreesToRadians(45) )
  camera.lens.near = .1
  camera.lens.far = 1000

  camera.position[1] = 2

  scene.add( camera )

}

function initControls(){

  orbitControls = new OrbitControls( renderer.canvas )
  maxControls   = new MaxControls( renderer.canvas )

  controls = orbitControls
  controls.start( camera )

}

function initDebug(){

  var opts = {
    setOrbitControls,
    setMaxControls,
  } 

  const f = GUI.addFolder( 'camera ctrl' )

  f.add( opts, 'setOrbitControls' )
  f.add( opts, 'setMaxControls' )

  function setOrbitControls(){
    controls.stop() 
    controls = orbitControls
    controls.start( camera ) 
  }
  function setMaxControls(){ 
    controls.stop()
    controls = maxControls
    controls.start( camera )
  }
  


}

function onMouseMove( evt ){

  mouse[0] = evt.pageX
  mouse[1] = evt.pageY

}


function render(){

  elapsedTime += RAF.dt
  
  gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT )

  scene.update()
  controls.update()

  camera.updateViewProjectionMatrix( renderer.width, renderer.height )

  // GRID
  grid.render( camera )

  // PLANE
  // plane.render( camera )
  // plane.material.prg.uTime( elapsedTime )

  
}

function resize( w, h ){

  renderer.resize( w, h )

}

function play(){ RAF.subscribe( rafId, render ) }
function pause(){ RAF.unsubscribe( rafId ) }

export default {
  init,
  play,
  pause,
  resize
}