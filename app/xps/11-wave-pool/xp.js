import Camera from 'nanogl-camera'
import Texture from 'nanogl/texture'

import AssetsStore from 'libs/assets-store'
import RAF from 'libs/raf'

import OrbitControls from 'camera/orbit-controls'
import MaxControls from 'camera/max-controls'

import WaveMaterial from './materials/wave-material'
import PlaneGeometry from 'geometries/plane-geometry'
import Mesh from 'entities/mesh'

import Grid from 'helpers/grid'
import GUI from 'helpers/gui'

import {createCanvas} from 'utils/dom-utils'
import {degreesToRadians} from 'utils/number-utils'

import Scene from './core/scene'
import Renderer from './core/renderer'
import Programs from './programs'

import Debug from './debug'

import { vec2, vec3, vec4, mat4 } from 'gl-matrix'


var rafId = 'raf-xp'

var gl
var elapsedTime = 0

var scene
var renderer

// camera
var camera
var controls
var orbitControls
var maxControls

// to render
var grid
var plane

// texture
var canvas
var circleTexture

var isPaused = false



function init( canvas ){
  
  renderer = new Renderer( canvas, {
    clearColor: [ 21 / 255, 90 / 255, 220 / 255 ]
  } )
  scene = new Scene()

  gl = renderer.gl
  gl.enable(gl.BLEND);
  gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

  initCircleTexture()
  initPrograms()
  initGrid()
  initPlane()
  initCamera()
  initControls()
  initDebug()

  // autoplay
  play()

  // window.addEventListener( 'mousemove', onMouseMove )

}

function initCircleTexture(){

  canvas = createCanvas( 128, 128 )
  canvas.style.position = 'absolute'
  canvas.style.left = '0px'
  canvas.style.top = '0px'
  canvas.style.zIndex = 10

  var ctx = canvas.getContext('2d')
  ctx.fillStyle = 'white'
  ctx.beginPath();
  ctx.arc( 64, 64, 64, 0, Math.PI * 2, false )
  ctx.fill()

  // debug
  // document.body.appendChild(canvas)

}

function initPrograms( ){

  Programs.init( gl )

}

function initGrid(){

  grid = new Grid( gl )
  scene.add( grid )

  grid.rotateX( Math.PI * .5 )

}

function initPlane(){

  var tex = new Texture( gl, gl.RGBA )
  tex.fromImage( canvas )

  var prg = Programs.get( 'wave' )
  var material = new WaveMaterial( gl, { prg, tex } )
  var geometry = new PlaneGeometry( gl, {
    width: 60,
    height: 60,
    widthSegments: 40,
    heightSegments: 20
  } )
  geometry.drawingMethod = 'drawPoints'

  plane = new Mesh( geometry, material )
  plane.x = 0
  plane.y = -0
  plane.z = -300
  // plane.position[2] = -30
  plane.rotateX( -Math.PI * .5 )
  plane.setScale( 10 )

  scene.add( plane )

}


function initCamera(){

  camera = Camera.makePerspectiveCamera()
  camera.lens.setAutoFov( degreesToRadians(45) )
  camera.lens.near = .1
  camera.lens.far = 1000

  camera.position[1] = 5

  scene.add( camera )

}

function initControls(){

  orbitControls = new OrbitControls( renderer.canvas )
  maxControls   = new MaxControls( renderer.canvas )

  controls = orbitControls
  controls.start( camera )
  controls.target[2] = 5

}

function initDebug(){

  var opts = {
    setOrbitControls,
    setMaxControls,
  } 

  const f = GUI.addFolder( 'camera ctrl' )

  f.add( opts, 'setOrbitControls' )
  f.add( opts, 'setMaxControls' )

  function setOrbitControls(){
    controls.stop() 
    controls = orbitControls
    controls.start( camera ) 
  }
  function setMaxControls(){ 
    controls.stop()
    controls = maxControls
    controls.start( camera )
  }
  


}

function onMouseMove( evt ){

  mouse[0] = evt.pageX
  mouse[1] = evt.pageY

}


function render(){

  elapsedTime += RAF.dt
  
  gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT )

  scene.update()
  controls.update()

  camera.updateViewProjectionMatrix( renderer.width, renderer.height )

  // GRID
  // grid.render( camera )

  // PLANE
  plane.render( camera )
  plane.material.prg.uTime( elapsedTime * 0.004 )
  // plane.material.prg.uM( plane._wmatrix )
  // plane.material.prg.uCamPosition( camera.position )

  
}

function resize( w, h ){

  renderer.resize( w, h )

}

function togglePause(){

  isPaused = !isPaused

  if( isPaused ) pause()
  else play()

}

function play(){ RAF.subscribe( rafId, render ) }
function pause(){ RAF.unsubscribe( rafId ) }

export default {
  init,
  play,
  pause,
  resize,
  togglePause
}