import { vec3, mat4 } from 'gl-matrix'

var M4 = mat4.create();

class WaveMaterial {

  /**
   * @param {Object} opts =>
   *   @param {Number} diffuseColor
   *   @param {Texture} diffuseMap
   *   @paran {Number} diffuseMapScale
   * 
   */
  constructor( gl, opts = {} ) {
    
      this.gl    = gl
      this.prg   = opts.prg || null
      
      this.color = opts.color || vec3.fromValues( 1, 1, 1 )
      this.tex   = opts.tex
      
      this.setUniforms()

  }

  setUniforms(){

    var prg = this.prg
    prg.use()

    prg.uTex( this.tex )
    
  }

  prepare( node, camera ) { 

    var prg = this.prg
    prg.use()

    this.tex.bind()
    prg.uTex( this.tex )
    
    camera.modelViewProjectionMatrix( M4, node._wmatrix )
    prg.uMVP( M4 )

  }

}

export default WaveMaterial