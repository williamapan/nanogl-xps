import Node from 'nanogl-node'

class Scene extends Node {

  constructor() {

    super()

  }

  update(){

    // UPDATE NODES'S WORLD MATRIX ( AND UPDATE CHILDREN )
    // ====================
    this.updateWorldMatrix()

  }

}

export default Scene