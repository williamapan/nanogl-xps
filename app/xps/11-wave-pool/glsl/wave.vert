attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUv;

uniform mat4 uMVP;
uniform mat4 uM;
uniform float uTime;
uniform vec3 uCamPosition;

varying vec3 vPosition;
varying vec3 vColor;

#pragma glslify: snoise     = require( './snoise2D.glsl' )

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(){

  float height = sin( uTime * 0.6 + aPosition.y / 12.0 ) * 3.;
  float pertubX = snoise( aPosition.xy * 100.0 ) * 0.05;
  float pertubY = snoise( aPosition.xy * 100.0 ) * 8.;

  vec3 newPos = vec3( aPosition.x + pertubX, aPosition.y + pertubY , height );

  gl_Position = uMVP * vec4( newPos, 1.0 );

  vPosition = aPosition;

  vec3 color1 = vec3( 21./255., 90./255., 220./255. );
  vec3 color2 = vec3( 1.0, 1.0, 1.0 );
  float c = snoise( aUv * 1000.0 ) * 0.01;
  c = step( 0.1, c*100.0 );
  vColor =  mix( color1, color2, c );


  vec4 model = uM * vec4( aPosition, 1.0 );
  float distToCamera = length( uCamPosition - aPosition );
  float uRatio = 705.0 / 2.0;
  float pointSize = 10.0 + abs(snoise( abs(aPosition.xy / 30. ) * 10.0 )) * 40.;//( ( 20.0 + rand( aPosition.xy ) * 80. ) );
  // float distCoeff = length( uCamPosition *  );
  gl_PointSize = pointSize * ( uRatio / gl_Position.z );//max( pointSize - distCoeff, 1.0 );

}