uniform float uTime;
uniform sampler2D uTex;

varying vec3 vColor;

void main() {

  
  vec2 uv = vec2( gl_PointCoord.x, gl_PointCoord.y );
  vec3 color = texture2D( uTex, uv ).r * vColor;
  float alpha = texture2D( uTex, uv ).r;

  gl_FragColor = vec4( color , alpha );

  uTime;
  uTex;

}