import Program from 'nanogl/program'
import Node from 'nanogl-node'
import Camera from 'nanogl-camera'
import OrbitControls from 'camera/orbit-controls'
import Plane from 'entities/plane'
import Cube from 'entities/cube'
import Grid from 'helpers/grid'
import {degreesToRadians} from 'utils/number-utils'
import {vec3} from 'gl-matrix'
var prg
var camera
var elapsedTime = 0

var programs = {}
var controls

class Scene {

  constructor( renderer ){

    this.renderer = renderer
    this.gl       = renderer.gl
    this.dt       = 1/60

    this.root     = new Node()

    this.init()

  }

  init(){

    var gl = this.gl

    this.initPrograms()
    this.initPlane()
    this.initCube()
    this.initGrid()
    this.initCamera()
    this.initControls()

    this.renderer._checkSize()

  }

  initPrograms(){

    var gl = this.gl
    var names = [
      'basic2D',
      'basic3D'
    ]

    for ( var i = 0; i < names.length; i++ ) {

      var prgName = names[i]

      var prg = new Program( gl )
      prg.compile(
        require( 'glsl/' +prgName+ '.vert' ),
        require( 'glsl/' +prgName+ '.frag' )
      )
      prg.use()

      programs[ prgName ] = prg

    }
    
  }

  initPlane(){

    this.plane = new Plane( programs[ 'basic2D' ] )
    this.root.add( this.plane )
    
    this.plane.position[1] = 1
    this.plane.position[0] = -4

  }

  initCube(){
    
    var gl = this.gl

    this.cube = new Cube( programs[ 'basic3D' ] )
    this.cube.position[1] = 1

    this.root.add( this.cube )
  }

  initGrid(){

    this.grid = new Grid( this.gl )
    this.root.add( this.grid )

    this.grid.rotateX( Math.PI * .5 )

  }

  initCamera(){

    camera = Camera.makePerspectiveCamera()
    camera.lens.setAutoFov( degreesToRadians(45) )
    camera.lens.near = .1
    camera.lens.far = 1000

    camera.position[1] = 2

    this.root.add( camera )

  }

  initControls(){ 

    controls = new OrbitControls( camera, this.renderer.canvas )

  }

  render( dt ){

    var gl = this.gl

    elapsedTime += dt

    // CAMERA
    // ====================
    controls.update()
    camera.updateViewProjectionMatrix( this.renderer.width, this.renderer.height )

    // UPDATE NODES'S WORLD MATRIX ( AND UPDATE CHILDREN )
    // ====================
    this.root.updateWorldMatrix()

    // RENDERING
    // ====================
    gl.bindFramebuffer(gl.FRAMEBUFFER, null)
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 )
    gl.viewport( 0, 0, this.renderer.width, this.renderer.height )
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT )
    
    this.grid.render( camera )

    this.cube.render( camera )
    this.cube.rotateY( .01 )
    this.cube.rotateX( .01 )

    this.plane.render( camera )
    this.plane.rotateY( .01 )
    
    // this.grid.prg.uTime( ( Math.sin(elapsedTime * 10) ) )

  }

}

export default Scene