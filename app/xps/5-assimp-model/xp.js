import Camera from 'nanogl-camera'
import Texture from 'nanogl/texture'

import AssetsStore from 'libs/assets-store'
import RAF from 'libs/raf'

import OrbitControls from 'camera/orbit-controls'

import BoxGeometry from 'geometries/box-geometry'
import BasicMaterial from 'materials/basic-material'

import Mesh from 'entities/mesh'

import Grid from 'helpers/grid'
import GUI from 'helpers/gui'

import Light from 'lights/light'

import {degreesToRadians} from 'utils/number-utils'

import Scene from './scene'
import Renderer from './renderer'
import Programs from './programs'

import Debug from './debug'

import AssimpGeometry from './assimp-geometry'
import { vec2, vec3, vec4, mat4 } from 'gl-matrix'


var rafId = 'raf-xp'

var gl
var elapsedTime = 0

var scene
var renderer

// camera
var camera
var controls

// to render
var grid
var ico


function init( canvas ){
  
  renderer = new Renderer( canvas )
  scene = new Scene()

  gl = renderer.gl

  initPrograms()
  initGrid()
  initIco()
  initCamera()
  initControls()
  initDebug()

  // autoplay
  play()

}

function initPrograms( ){

  Programs.init( gl )

}

function initGrid(){

  grid = new Grid( gl )
  scene.add( grid )

  grid.rotateX( Math.PI * .5 )

}

function initIco(){
  var data = AssetsStore.get( 'ico.json' )
  var prg = Programs.get( 'basicMaterial' )
  var material = new BasicMaterial( gl, { diffuseColor: [ 1, 1, 1 ] } )
  var geometry = new AssimpGeometry( gl, data )

  ico = new Mesh( geometry, material )
  ico.position[0] = .5
  ico.position[1] = .5
  // ico.setScale( 0.001 )

  scene.add( ico )

}

function initCamera(){

  camera = Camera.makePerspectiveCamera()
  camera.lens.setAutoFov( degreesToRadians(45) )
  camera.lens.near = .1
  camera.lens.far = 1000

  camera.position[1] = 2

  scene.add( camera )

}

function initControls(){

  controls = new OrbitControls( camera, renderer.canvas )

}

function initDebug(){


}


function render(){

  elapsedTime += RAF.dt
  
  gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT )

  scene.update()
  controls.update()

  camera.updateViewProjectionMatrix( renderer.width, renderer.height )

  // GRID
  grid.render( camera )

  ico.render( camera )

}

function resize( w, h ){

  renderer.resize( w, h )

}

function play(){ RAF.subscribe( rafId, render ) }
function pause(){ RAF.unsubscribe( rafId ) }

export default {
  init,
  play,
  pause,
  resize
}