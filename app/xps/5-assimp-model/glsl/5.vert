attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUv;

uniform mat4 uMVP;

varying vec2 vUv;
varying vec3 vNormal;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(){

  float scale = rand(aPosition.xy) * 10.0;
  vec3 direction = aNormal * scale;
  vec3 position = aPosition + direction;

  gl_Position = uMVP * vec4( position, 1.0 );

  vUv = aUv;
  vNormal = aNormal;

}