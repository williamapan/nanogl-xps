uniform vec3 uDiffuseColor;

#ifdef DIFFUSE_MAP
  uniform sampler2D uDiffuseMap;
  uniform float uDiffuseMapScale;
  uniform float uDiffuseMapOffsetX;
  uniform float uDiffuseMapOffsetY;
#endif

varying vec2 vUv;

void main(){

  vec3 color = uDiffuseColor;

  #ifdef DIFFUSE_MAP
    vec2 uv =  vec2( vUv.x - uDiffuseMapOffsetX, vUv.y - uDiffuseMapOffsetY );
    color = texture2D( uDiffuseMap, vUv * uDiffuseMapScale ).rgb;
  #endif

  gl_FragColor = vec4( color, 1.0 ); 

}