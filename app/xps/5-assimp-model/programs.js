import Program from 'nanogl/program'
import GLUtils from 'utils/gl-utils'

var gl
var defs     = ''
var programs = {}

/**
 * Get current webgl context to play with
 * List all programs for easy readability
 * 
 * @param {WebGLRenderingContext} _gl
 */
function init( _gl ) {

  gl = _gl

  defs     = 'precision ' + GLUtils.getPrecision(gl) + ' float;\n'

  programs[ 'basic2D' ]       = new Program( gl )
  programs[ 'basic3D' ]       = new Program( gl )
  programs[ 'basicMaterial' ] = new Program( gl )

  _compilePrograms()

}

/**
 * Returns a program given its id
 * 
 * @param {String} programId
 */
function get( programId ){

  if ( programs[ programId ] ) return programs[ programId ]

  console.error( `[Programs get] program '${programId}' doesnt exist` )

}



/**
 * Compile all programs
 */
function _compilePrograms() {

  programs[ 'basic2D' ].compile(
    require( 'glsl/basic2D.vert' ),
    require( 'glsl/basic2D.frag' ),
    defs
  )

  programs[ 'basic3D' ].compile(
    require( 'glsl/basic3D.vert' ),
    require( 'glsl/basic3D.frag' ),
    defs
  )

  programs[ 'basicMaterial' ].compile(
    require('./glsl/5.vert'),
    require('./glsl/5.frag'),
    defs
  )


  programs[ 'basic2D' ].use()
  programs[ 'basic3D' ].use()
  programs[ 'basicMaterial' ].use()

  console.log( '\n' )
  console.log('programs available : ')
  console.log( programs )
  console.log( '\n' )

}


export default {
  init,
  get
}