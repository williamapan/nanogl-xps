import ArrayBuffer from 'nanogl/arraybuffer'
import IndexBuffer from 'nanogl/indexbuffer'

class AssimpGeometry {

  constructor( gl, data ) {
    
    this.gl = gl

    var d = data.meshes[0]

    this.vertices = new Float32Array( d.vertices )
    this.indices = new Uint16Array( [].concat.apply([], d.faces) )
    this.normals = new Float32Array( d.normals )

    this.allocate()

    this.drawingMethod = 'drawTriangles'
    // this.drawingMethod = 'drawLines'

  }

  allocate(){

    var gl = this.gl

    this.vbuffer = new ArrayBuffer( gl )
    this.vbuffer.data( this.vertices )
    this.vbuffer.attrib( 'aPosition', 3, gl.FLOAT )

    this.nbuffer = new ArrayBuffer( gl )
    this.nbuffer.data( this.normals )
    this.nbuffer.attrib( 'aNormal', 3, gl.FLOAT )

    this.ibuffer = new  IndexBuffer( gl, gl.UNSIGNED_SHORT )
    this.ibuffer.data( this.indices )

  }

  bind( prg ){

    var gl = this.gl

    this.vbuffer.attribPointer( prg )
    // this.vbuffer.attrib( 'aPosition', 3, gl.FLOAT )

    this.nbuffer.attribPointer( prg )
    // this.nbuffer.attrib( 'aNormal', 3, gl.FLOAT )
    
    this.ibuffer.bind()

  }

  render(){

    this.vbuffer[ this.drawingMethod ]()

  }

}

export default AssimpGeometry