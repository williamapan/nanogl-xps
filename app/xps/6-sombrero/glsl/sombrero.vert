attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUv;

uniform mat4 uMVP;
uniform float uTime;

varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;

float map(float value, float inMin, float inMax, float outMin, float outMax) {
  return outMin + (outMax - outMin) * (value - inMin) / (inMax - inMin);
}

void main(){

  float cLength = 1.0 - length(aPosition.xy);
  float distance = distance( aPosition.xy, vec2(0.0) );

  // if ( distance <= 0.5 ) {
  //     distance = map(distance, 0.0, 1.0, 0.5, 0.0) * 0.1;
  // }else {
  //     distance = 0.0;
  // }

  float time = cos( uTime * 0.0025 );
  vec2 uv = aPosition.xy * sin( cLength * 20.0 - time );
  float x = aPosition.x  * 10.0;//pow( aPosition.x, 2.0 );
  float y = aPosition.y  * 10.0;//pow( aPosition.y, 2.0 );
  float z = sin( x*x + y*y ) / (x*x + y*y);

  vec3 newPos = vec3( aPosition.x, aPosition.y, z * time * .2  );
  // vec3 newPos = vec3( aPosition.x, aPosition.y, 1.0 - distance  );

  gl_Position = uMVP * vec4( newPos, 1.0 );

  gl_PointSize = 5.0;

  vPosition = aPosition;
  vNormal = aNormal;
  vUv = aUv;

}