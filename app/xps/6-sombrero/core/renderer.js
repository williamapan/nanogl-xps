var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1

var contextOpts = {
  depth:                        true,
  stencil:                      true,
  antialias:                    true,
  alpha:                        false,
  premultipliedAlpha:           false,
  preserveDrawingBuffer:        false,
  failIfMajorPerformanceCaveat: false
}

/**
 * Init webgl context
 * 
 * @param {HTMLElement} canvas
 * @param {Object} [opts]
 *            {Number} width
 *            {Number} height
 *            {Number} resolution
 *            {Array} clearColor
 */
class Renderer {

  constructor( canvas, opts = {} ) {
    
    this.width      = opts.width || window.innerWidth
    this.height     = opts.height || window.innerHeight
    this.resolution = opts.resolution || ( isFirefox && contextOpts.antialias ) ? 2 : 1

    this.canvas = canvas
    this.gl     = canvas.getContext( 'webgl', contextOpts )

    this.gl.enable( this.gl.DEPTH_TEST )

    var color = opts.clearColor || [ 0.05, 0.05, 0.05 ]
    this.gl.clearColor( color[0], color[1], color[2], 1.0 )

    this.resize( this.width, this.height )

  }

  resize( w, h ){

    this.width  = w * this.resolution
    this.height = h * this.resolution

    this.canvas.width        = this.width
    this.canvas.height       = this.height
    this.canvas.style.width  = this.width + 'px'
    this.canvas.style.height = this.height + 'px'

    this.gl.viewport( 0, 0, this.width, this.height )

    var scaleTransform                = `scale(${1/this.resolution})`
    this.canvas.style.webkitTransform = scaleTransform
    this.canvas.style.transform       = scaleTransform
    this.canvas.style.transformOrigin = '0 0'

  }

}

export default Renderer
