import Program        from 'nanogl/program'
import Node           from 'nanogl-node'
import Texture        from 'nanogl/texture'
import Camera         from 'nanogl-camera'

import OrbitControls  from 'camera/orbit-controls'

import PlaneGeometry  from 'geometries/plane-geometry'
import BoxGeometry    from 'geometries/box-geometry'

import BasicMaterial  from 'materials/basic-material'

import Mesh           from './mesh-outline'
import Plane          from 'entities/plane'
import Cube           from 'entities/cube'
import Grid           from 'helpers/grid'

import {
  degreesToRadians,
  map
}                     from 'utils/number-utils'
import {vec3}         from 'gl-matrix'

var prg
var camera
var elapsedTime = 0

var programs = {}
var controls

window.cameraOpts = {
  auto: false,
  fov: 110,
  minFovAuto: 40,
  maxFovAuto: 102,
  shakeSpeed: 16,
  minShakeSpeed: 0,
  maxShakeSpeed: 5,
  shakeAmplitude: .05,
  minShakeAmplitude: 0,
  maxShakeAmplitude: .2
}

var stencilPrg


class Scene {

  constructor( renderer ){

    this.renderer = renderer
    this.gl       = renderer.gl
    this.dt       = 1/60

    this.root     = new Node()

    this.init()

  }
  

  init(){

    var gl = this.gl

    this.initPrograms()
    this.initOuterCube()
    this.initInnerCube()
    this.initGrid()
    this.initCamera()
    this.initControls()
    this.initStencil()
    this.initDebug()

    this.renderer._checkSize()

  }

  initPrograms(){

    var gl = this.gl
    var names = [
      'basic2D',
      'basic3D'
    ]

    for ( var i = 0; i < names.length; i++ ) {

      var prgName = names[i]

      var prg = new Program( gl )
      prg.compile(
        require( 'glsl/' +prgName+ '.vert' ),
        require( 'glsl/' +prgName+ '.frag' )
      )
      prg.use()

      programs[ prgName ] = prg

    }
    
  }

  initOuterCube(){

    var img = new Image()
    img.onload = onloaded.bind(this)
    img.src = 'dots.jpg'
    function onloaded(){ diffuseMap.fromImage( img ) }

    var diffuseMap = new Texture( this.gl )
    diffuseMap.fromData( 128, 128 )

    var material = new BasicMaterial( this.gl, { 
      diffuseColor: [ 0, 1, 1 ],
      diffuseMap,
      diffuseMapScale: 6.23
    } )

    this.outerCube = new Cube( material )
    this.outerCube.setScale( 20 )
    this.outerCube.position[0] = 10
    this.outerCube.position[1] = 10
    this.outerCube.position[2] = 10

    this.root.add( this.outerCube )

  }

  initInnerCube(){
    
    var geometry = new BoxGeometry( this.gl )

    var img = new Image()
    img.onload = onloaded.bind(this)
    img.src = 'dots.jpg'
    function onloaded(){ diffuseMap.fromImage( img ) }

    var diffuseMap = new Texture( this.gl )
    diffuseMap.fromData( 128, 128 )

    var material = new BasicMaterial( this.gl, { 
      diffuseColor: [ 0, 1, 1 ],
      diffuseMap,
      diffuseMapScale: 1
    } )

    this.innerCube = new Mesh( geometry, material )
    this.innerCube.position[1] = 1.5
    this.innerCube.position[0] = 1.5
    this.innerCube.position[2] = 1.5
    this.innerCube.setScale( 3 )

    this.root.add( this.innerCube )
  }

  initGrid(){

    this.grid = new Grid( this.gl )
    this.root.add( this.grid )

    this.grid.rotateX( Math.PI * .5 )

  }

  initCamera(){

    camera = Camera.makePerspectiveCamera()
    camera.lens.setAutoFov( degreesToRadians( cameraOpts.fov ) )
    camera.lens.near = .1
    camera.lens.far = 1000

    camera.position[1] = 2

    this.root.add( camera )
    

  }

  initControls(){ 

    controls = new OrbitControls( camera, this.renderer.canvas )

  }

  initStencil(){

    stencilPrg = new Program(this.gl)
    stencilPrg.compile(
      require('./stencil.vert'),
      require('./stencil.frag')
    )
    stencilPrg.use()


  }

  initDebug(){
    
    var settings = QuickSettings.create(0, 0, "Settings" )

    var cameraSettings = QuickSettings.create( 'inherit', 0, 'Camera', settings._panel.querySelector('.qs_content'))
    cameraSettings._panel.style.top = 'inherit'
    cameraSettings.addRange('fov', 10, 180, 45, 0.1, updateFov);
    cameraSettings.bindRange( 'minFovAuto', 10, 60, 40, 1, cameraOpts )
    cameraSettings.bindRange( 'maxFovAuto', 61, 180, 102, 1, cameraOpts )
    cameraSettings.bindRange( 'shakeSpeed', 0, 40, 8, .1, cameraOpts )
    cameraSettings.bindRange( 'shakeAmplitude', 0, 1, .1, .01, cameraOpts )
    cameraSettings.bindBoolean( 'auto', false, cameraOpts )
    // cameraSettings.collapse();
    function updateFov( newFov ){
      cameraOpts.fov = newFov
      camera.lens.setAutoFov( degreesToRadians(cameraOpts.fov) )
    }

    var controlsSettings = QuickSettings.create( 'inherit', 0, 'Orbit controls', cameraSettings._panel.querySelector('.qs_content'))
    controlsSettings._panel.style.top = 'inherit'
    controlsSettings.bindRange( 'minRadius', .1, 10, 3, .1, controls )
    controlsSettings.bindRange( 'maxRadius', 11, 100, 50, .1, controls )
    


  }

  render( dt ){

    var gl = this.gl

    elapsedTime += dt

    if ( elapsedTime > 1000 ) elapsedTime = 0

    // console.log(elapsedTime)

    var uTime = Math.abs( Math.sin(elapsedTime * .9) )

    // CAMERA
    // ====================
    controls.update()
    camera.updateViewProjectionMatrix( this.renderer.width, this.renderer.height )

    if ( cameraOpts.auto ) {
      // controls.radius = map( uTime, 0, 1, 6, 10 )
      controls.radius = map( uTime, 0, 1, 5.6, 16.6 )
      cameraOpts.fov = map( uTime, 0, 1, 120, 45 )
      // console.log(cameraOpts.fov)
      // console.log(controls.radius)
      camera.lens.setAutoFov( degreesToRadians( cameraOpts.fov ) )
      var shakeSpeed = cameraOpts.shakeSpeed
      var shakeAmplitude = map( uTime, 0, 1, .1, .0035 )
      controls.thetaOffset = ( Math.sin( (elapsedTime) * shakeSpeed) * shakeAmplitude )
    }    

    // UPDATE NODES'S WORLD MATRIX ( AND UPDATE CHILDREN )
    // ====================
    this.root.updateWorldMatrix()

    // RENDERING
    // ====================
    gl.bindFramebuffer(gl.FRAMEBUFFER, null)
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 )
    gl.viewport( 0, 0, this.renderer.width, this.renderer.height )
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT | gl.STENCIL_BUFFER_BIT )

    // this.grid.render( camera )

    // innercube
    // gl.clear(0)
    gl.enable(gl.STENCIL_TEST)
    gl.stencilFunc( gl.ALWAYS, 1, 0xFF )
    gl.stencilOp( gl.REPLACE, gl.REPLACE, gl.REPLACE )
    this.innerCube.setScale( 3 )
    this.innerCube.updateWorldMatrix()
    // this
    
    this.innerCube.render( camera )
    // this.innerCube.material.prg.uDiffuseMapScale( 1 + 1 * ( uTime * 1 ) )
    
    // outline
    if ( cameraOpts.auto ) {

      gl.enable(gl.STENCIL_TEST)
      gl.stencilFunc( gl.NOTEQUAL, 1, 0xFF )
      gl.stencilOp( gl.KEEP, gl.KEEP, gl.KEEP )
      this.innerCube.setScale( 3.1 )
      this.innerCube.updateWorldMatrix()
      this.innerCube.renderOutline( camera, stencilPrg, uTime )
      gl.disable(gl.STENCIL_TEST)

    }
    
    
    // outercube
    this.outerCube.render( camera )
    // this.outerCube.material.prg.uDiffuseMapScale( 10 + 10 * ( uTime * 1 ) )
    
    // this.grid.prg.uTime( ( Math.sin(elapsedTime * 10) ) )


  }

}

export default Scene