precision highp float;

uniform float uTime;

void main(){

  vec3 color2 = vec3( 0.662745098, 0.290196078, 0.337254902 );
  vec3 color1 = vec3( 0.109803922, 0.101960784, 0.109803922 );

  vec3 mixColor = mix( color2, color1, clamp( uTime + .2, 0.0, 1.0 ) );

  gl_FragColor = vec4( mixColor, 1.0 );

}