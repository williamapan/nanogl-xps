struct Material {
  vec3 ambient;
  vec3 diffuse;
  sampler2D diffuseMap;
  vec3 specular;
  sampler2D specularMap;
  float shininess;
};

struct AmbientLight {
  float intensity;
  vec3 ambient;
};

struct Light {
  float intensity;
  vec3 position;
  vec3 diffuse;
  vec3 specular;
};

uniform vec3 uCameraPosition;
uniform mat3 uNormalMatrix;
uniform Material uMaterial;
uniform AmbientLight uAmbientLight;
uniform Light uLight;

varying vec2 vUv;
varying vec3 vNormal;
varying vec3 vFragPosition;

// ---------------------------------

// GETTERS

// ---------------------------------

vec3 getNormal(){

  return normalize( uNormalMatrix * vNormal );

}

vec3 getMaterialAmbient(){

  vec3 ambient = uMaterial.ambient;

  #ifdef DIFFUSE_MAP
    ambient = texture2D(uMaterial.diffuseMap, vUv ).rgb;
  #endif

  return ambient;

}

// ---------------------------------

// MODIFIERS

// ---------------------------------


// APPLY AMBIENT
// --------
void applyAmbient( inout vec3 color ) {

  color *= uAmbientLight.ambient * uAmbientLight.intensity;

}

// APPLY DIFFUSE
// --------
void applyDiffuse( inout vec3 color ) {

  vec3 normal     = getNormal();
  vec3 lightDir   = normalize( uLight.position - vFragPosition );
  float incidence = max( dot( normalize(normal), lightDir ), 0.0 );
  vec3 lightDiffuse    = incidence * uLight.diffuse * uLight.intensity;  
  vec3 materialDiffuse = uMaterial.diffuse;
  
  #ifdef DIFFUSE_MAP
    materialDiffuse = texture2D(uMaterial.diffuseMap, vUv ).rgb;
  #endif
  
  color += lightDiffuse * materialDiffuse;

}

// APPLY SPECULAR
// ---------
void applySpecular( inout vec3 color ) {

  vec3 normal           = getNormal();
  vec3 lightDir         = normalize( uLight.position - vFragPosition );
  vec3 viewDir          = normalize( uCameraPosition - vFragPosition );
  vec3 reflectDir       = reflect( -lightDir, normal );

  float spec            = pow( max( dot(viewDir, reflectDir), 0.0 ), uMaterial.shininess );
  vec3 specular         = spec * uLight.specular * uLight.intensity;
  
  vec3 materialSpecular = uMaterial.specular;
  #ifdef DIFFUSE_MAP
    materialSpecular = texture2D(uMaterial.specularMap, vUv ).rgb * 2.0;
  #endif

  color += specular * materialSpecular;

}

// ---------------------------------

// MAIN

// ---------------------------------
void main(){

  vec3 color   = getMaterialAmbient();

  applyAmbient( color );
  applyDiffuse( color );
  applySpecular( color );

  gl_FragColor = vec4( color, 1.0 ); 

}