attribute vec3 aPosition;
attribute vec2 aUv;
attribute vec3 aNormal;

uniform mat4 uMVP;
uniform mat4 uProjectionMatrix;
uniform mat4 uModelMatrix;

varying vec2 vUv;
varying vec3 vNormal;
varying vec3 vFragPosition;


void main(){

  gl_Position = uMVP * vec4( aPosition, 1.0 );

  vUv = aUv;
  vNormal = aNormal;
  vFragPosition = vec3( uModelMatrix * vec4( aPosition, 1.0 ) );

}