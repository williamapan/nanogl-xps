attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUv;

uniform mat4 uMVP;

varying vec2 vUv;
varying vec3 vNormal;


void main(){

  gl_Position = uMVP * vec4( aPosition, 1.0 );

  vUv = aUv;
  vNormal = aNormal;

}