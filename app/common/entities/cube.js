import Node from 'nanogl-node'
import BoxGeometry from 'geometries/box-geometry'


class Cube extends Node {

  constructor( material ){

    super()

    this.gl = material.gl

    this.material = material
    
    this.geometry = new BoxGeometry( this.gl )    
      
  }

  render( camera ){

    this.material.prepare( this, camera )

    this.geometry.bind( this.material.prg )
    this.geometry.render()

  }

}

export default Cube