import {vec3} from 'gl-matrix'
import {limit} from 'utils/number-utils'

var target = vec3.create()
var mouseDownOrigin = [ 0, 0 ]

var phi = Math.PI / 2  - .5
var theta = Math.PI * 2

var lastPhi = 0
var lastTheta = 0

var phiOffset = 0
var thetaOffset = 0

var lastNodePosition
var lastTargetPosition

class OrbitControls {

  /**
   *
   * @constructor
   *
   * @param {nanogl-node} Node
   * @param {HTMLElement} domEl
   */
  constructor( domEl ) {

    this.domEl = domEl || window;

    this.radius = 5
    this.minRadius = 1
    this.maxRadius = 50

    this._onMouseWheel = this.__onMouseWheel.bind(this)
    this._onMouseDown  = this.__onMouseDown.bind(this)
    this._onMouseMove  = this.__onMouseMove.bind(this)
    this._onMouseUp    = this.__onMouseUp.bind(this)
    this._onContextMenu    = this.__onContextMenu.bind(this)

    this.target = vec3.create()

  }

  /* -------------------------

   PUBLIC

   -------------------------- */

  /**
   * Add event listeners
   * - click
   * - mousewheel
   */
  addListeners(){

    this.domEl.addEventListener( 'mousedown', this._onMouseDown )
    this.domEl.addEventListener( 'mousewheel' , this._onMouseWheel)
    this.domEl.addEventListener( 'DOMMouseScroll' , this._onMouseWheel)
    document.addEventListener( 'contextmenu', this._onContextMenu )

  }

  /**
   * Remove event listeners
   * - click
   * - mousewheel
   */
  removeListeners(){

    this.domEl.removeEventListener( 'mousedown', this._onMouseDown )
    this.domEl.removeEventListener('mousewheel', this._onMouseWheel)
    this.domEl.removeEventListener('DOMMouseScroll', this._onMouseWheel)
    document.removeEventListener( 'contextmenu', this._onContextMenu )

  }

  /**
   * Set camera
   *
   * @param {Camera} camera
   *
   */
  start( camera ) {

    this.addListeners()

    this.node = camera

  }

  /**
   * Look at target
   */
  update() {

    var x = this.radius * Math.sin( phi + phiOffset ) * Math.sin( theta + thetaOffset );
    var y = this.radius * Math.cos( phi + phiOffset );
    var z = this.radius * Math.sin( phi + phiOffset ) * Math.cos( theta + thetaOffset );

    this.node.position[0] = x
    this.node.position[1] = y
    this.node.position[2] = z

    this.node.lookAt( this.target );

  }

  stop(){

    this.removeListeners()

    this.el = null
    this.node = null

  }

  /* -------------------------

   PRIVATE

   -------------------------- */


  /**
   * Triggered whem user clicks on window
   *
   * @param evt
   * @private
   */
  __onMouseDown( evt ) {

    // evt.preventDefault();

    mouseDownOrigin[0] = evt.pageX;
    mouseDownOrigin[1] = evt.pageY;

    var p = this.node.position

    lastNodePosition = vec3.fromValues( p.x, p.y, p.z )
    lastTargetPosition = vec3.clone( target );
    lastPhi = phi;
    lastTheta = theta;

    this.domEl.addEventListener( 'mousemove', this._onMouseMove );
    this.domEl.addEventListener( 'mouseup', this._onMouseUp );

  }

  /**
   * Triggered whem user moves mouse on window
   *
   * @param evt
   * @private
   */
  __onMouseMove( evt ) {

    // evt.preventDefault();


    if ( evt.button === 0 ) {
      this._handleRotation( evt );
    }
    else {
      this._handlePanning( evt );
    }

    return false

  }

  /**
   * Triggered whem user mouse up on window
   *
   * @param evt
   * @private
   */
  __onMouseUp( evt ) {

    // evt.preventDefault();

    this.domEl.removeEventListener( 'mousemove', this._onMouseMove );
    this.domEl.removeEventListener( 'mouseup', this._onMouseUp );

  };

  /**
   * Triggered whem user scrolls with mouse
   *
   * @param evt
   * @private
   */
  __onMouseWheel( evt ) {

    var delta = evt.wheelDelta ? -evt.wheelDelta / 1000 : evt.detail / 10;

    this.radius += delta;
    this.radius = limit( this.radius, this.minRadius, this.maxRadius )

  };

  __onContextMenu( evt ) {

    if ( evt.button === 2 ) {
      evt.preventDefault();
      this.__onMouseDown( evt );
      return false;
    }

  }

  /**
   * Triggered by this.onMouseMove
   *
   * @param {Object} evt - mouse event
   * @private
   */
  _handleRotation( evt ) {

    var offsetX = ( mouseDownOrigin[0] - evt.pageX ) * 0.01;
    var offsetY = ( mouseDownOrigin[1] - evt.pageY ) * 0.01;

    phi = lastPhi + offsetY;
    theta = lastTheta + offsetX

    if ( phi >= Math.PI ) phi = Math.PI;
    if ( phi <= 0.001 ) phi = 0.001;

    if ( theta >= Math.PI * 2 ) 0;
    if ( theta <= 0 ) theta = Math.PI * 2;

  }

  /**
   * Triggered by this.onMouseMove
   *
   * @param {Object} evt - mouse event
   * @private
   */
  _handlePanning( evt ) {

    var offsetX = ( mouseDownOrigin[0] - evt.pageX ) * 0.01;
    var offsetY = ( mouseDownOrigin[1] - evt.pageY ) * 0.01;

    vec3.set( target, ( lastTargetPosition[0] - offsetX ) ,  lastTargetPosition[1] - offsetY,  lastTargetPosition[2] );
    vec3.set( this.node.position, ( lastNodePosition[0] - offsetX ), lastNodePosition[1] - offsetY, lastNodePosition[2] )
  }

  
  get phi () { return phi }
  get theta() { return theta }
  get phiOffset () { return phiOffset }
  get thetaOffset() { return thetaOffset }
  set phi( value ) { phi = value }
  set theta( value ) { theta = value }
  set phiOffset( value ) { phiOffset = value }
  set thetaOffset( value ) { thetaOffset = value }

}

export default OrbitControls

