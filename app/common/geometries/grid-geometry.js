import Geometry from 'geometries/geometry'

var STRIDE = 2
var BYTES  = Float32Array.BYTES_PER_ELEMENT

var vertices = []
var indices  = []

function _build( size, count ){

  var step = size / count

  // horizontal
  for ( var y = -size/2; y <= size/2; y += step ) {

    for ( var x = -size/2; x < size/2 - step; x += step ) {

      var _x = Math.floor( x * 10 ) / 10
      var _y = Math.floor( y * 10 ) / 10

      var x0 = parseFloat( x.toFixed(1) ) 
      var y0 = parseFloat( y.toFixed(1) )
      var x1 = parseFloat( (x+step).toFixed(1) )
      var y1 = parseFloat( y.toFixed(1) )

      vertices.push( x0 )
      vertices.push( y0 )
      vertices.push( x1 )
      vertices.push( y1 )

    }

  }
  
  // vertical
  for ( var x = -size/2; x <= size/2; x += step ) {

    for ( var y = -size/2; y < size/2 - step; y += step ) {

      var x0 = parseFloat( x.toFixed(1) )  
      var y0 = parseFloat( y.toFixed(1) ) 
      var x1 = parseFloat( x.toFixed(1) ) 
      var y1 = parseFloat( (y+step).toFixed(1) ) 

      vertices.push( x0 )
      vertices.push( y0 )
      vertices.push( x1 )
      vertices.push( y1 )

    }

  }

}

class GridGeometry extends Geometry {

  /**
   * Creates an instance of GridGeometry.
   * 
   * @param {WebGLContext} gl
   * @param {number} [size=50]
   * @param {number} [sub=50]
   */
  constructor( gl, size = 50, sub = 50 ) {

    _build( size, sub )

    super( gl, vertices )

    this.drawingMethod = 'drawLines'

  }

}

export default GridGeometry