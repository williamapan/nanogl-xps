import ArrayBuffer from 'nanogl/arraybuffer'
import IndexBuffer from 'nanogl/indexbuffer'

var vertices = []
var indices  = []
var normals  = []
var uvs = []

// from THREE.js
function _build( opts ) {
  
  var width = opts.width || 10
  var height = opts.height || 10
  var widthSegments = opts.widthSegments || 200
  var heightSegments = opts.heightSegments || 200
  
  var width_half = width / 2;
	var height_half = height / 2;

	var gridX = Math.floor( widthSegments ) || 1;
	var gridY = Math.floor( heightSegments ) || 1;

	var gridX1 = gridX + 1;
	var gridY1 = gridY + 1;

	var segment_width = width / gridX;
	var segment_height = height / gridY;

	vertices = new Float32Array( gridX1 * gridY1 * 3 );
	normals = new Float32Array( gridX1 * gridY1 * 3 );
	uvs = new Float32Array( gridX1 * gridY1 * 2 );

	var offset = 0;
	var offset2 = 0;

	for ( var iy = 0; iy < gridY1; iy ++ ) {

		var y = iy * segment_height - height_half;

		for ( var ix = 0; ix < gridX1; ix ++ ) {

			var x = ix * segment_width - width_half;

			vertices[ offset ] = x;
			vertices[ offset + 1 ] = - y;

			normals[ offset + 2 ] = 1;

			uvs[ offset2 ] = ix / gridX;
			uvs[ offset2 + 1 ] = 1 - ( iy / gridY );

			offset += 3;
			offset2 += 2;

		}

	}

	offset = 0;

	indices = new ( ( vertices.length / 3 ) > 65535 ? Uint32Array : Uint16Array )( gridX * gridY * 6 );

	for ( var iy = 0; iy < gridY; iy ++ ) {

		for ( var ix = 0; ix < gridX; ix ++ ) {

			var a = ix + gridX1 * iy;
			var b = ix + gridX1 * ( iy + 1 );
			var c = ( ix + 1 ) + gridX1 * ( iy + 1 );
			var d = ( ix + 1 ) + gridX1 * iy;

			indices[ offset ] = a;
			indices[ offset + 1 ] = b;
			indices[ offset + 2 ] = d;

			indices[ offset + 3 ] = b;
			indices[ offset + 4 ] = c;
			indices[ offset + 5 ] = d;

			offset += 6;

		}

	}

}

class PlaneGeometry {

  constructor( gl, opts ) {

    _build( opts )

    this.gl = gl

    this.vertices = vertices
    this.normals  = normals
    this.uvs      = uvs
    this.indices  = indices

    this.drawingMethod = 'drawTriangles'
		// this.drawingMethod = 'drawPoints'

    this.allocate()

  }

  allocate(){

    var gl = this.gl

    this.buffer = new ArrayBuffer( gl )
    this.buffer.data( this.vertices )
    this.buffer.attrib( 'aPosition', 3, gl.FLOAT )

    this.uvbuffer = new ArrayBuffer( gl )
    this.uvbuffer.data( this.uvs )
    this.uvbuffer.attrib( 'aUv', 2, gl.FLOAT ) 

    this.nbuffer = new ArrayBuffer( gl )
    this.nbuffer.data( this.normals )
    this.nbuffer.attrib( 'aNormal', 3, gl.FLOAT )

    this.ibuffer = new IndexBuffer( gl, gl.UNSIGNED_SHORT )
    this.ibuffer.data( this.indices )

  }

  bind( prg ) {
    
    var gl = this.gl 

    this.buffer.attribPointer( prg )
    this.uvbuffer.attribPointer( prg )
    this.nbuffer.attribPointer( prg )
    this.ibuffer.bind()

  }

  render() {

    this.ibuffer[ this.drawingMethod ]()

  }

}

export default PlaneGeometry