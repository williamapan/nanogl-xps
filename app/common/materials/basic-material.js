import { vec3, mat4 } from 'gl-matrix'
import Texture from 'nanogl/texture'
import Program from 'nanogl/program'
import GLUtils from 'utils/gl-utils'

var M4 = mat4.create();

class BasicMaterial {

  /**
   * @param {Object} opts =>
   *   @param {Number} diffuseColor
   *   @param {Texture} diffuseMap
   *   @paran {Number} diffuseMapScale
   * 
   */
  constructor( gl, opts = {} ) {
    
      this.gl   = gl
      this.defs = 'precision ' + GLUtils.getPrecision(gl) + ' float;\n'
      this.prg  = opts.prg || null
      
      this.diffuseColor = opts.diffuseColor || vec3.fromValues( 1, 1, 1 )
      this.diffuseMap   = opts.diffuseMap || null  
      this.diffuseMapScale = opts.diffuseMapScale || 1
      this.diffuseMapUVOffsetX = opts.diffuseMapOffsetX || 10
      this.diffuseMapUVOffsetY = opts.diffuseMapOffsetY || 0
      
      this.computeDefs()
      if ( !this.prg )  this.compileProgram()
      this.setUniforms()

  }

  computeDefs(  ){

    if ( this.diffuseMap ) {
      this.defs += '#define DIFFUSE_MAP\n'  
    }

  }

  compileProgram() {

    this.prg = new Program( this.gl )
    this.prg.compile(
      require('glsl/basic-material.vert'),
      require('glsl/basic-material.frag'),
      this.defs
    )

  }

  setUniforms(){

    var prg = this.prg
    prg.use()
    
    prg.uDiffuseColor( this.diffuseColor )

    if ( this.diffuseMap ) {
      prg.uDiffuseMapScale( this.diffuseMapScale )
      prg.uDiffuseMapOffsetX( this.diffuseMapUVOffsetX )
      prg.uDiffuseMapOffsetY( this.diffuseMapUVOffsetY )
    }

  }

  prepare( node, camera ) { 

    var prg = this.prg
    prg.use()

    camera.modelViewProjectionMatrix( M4, node._wmatrix )
    prg.uMVP( M4 )

    if ( this.diffuseMap ) {
      this.diffuseMap.bind()
    }

  }

}

export default BasicMaterial