import { vec3, mat3, mat4 } from 'gl-matrix'
import Texture from 'nanogl/texture'
import Program from 'nanogl/program'
import GLUtils from 'utils/gl-utils'

var M4 = mat4.create()
var NORMAL_MATRIX = mat3.create()

class PhongMaterial {

  /**
   * @param {Object} opts =>
   *   @param {Array} diffuse
   *   @param {Array} diffuseMap
   *   @param {Array} specular
   *   @param {Array} specularMap
   */
  constructor( gl, opts ) {
    
      this.gl   = gl
      this.defs = 'precision ' + GLUtils.getPrecision(gl) + ' float;\n'
      this.prg  = opts.prg || null

      this.normalMatrix = mat3.create();

      this.diffuse     = opts.diffuse || vec3.fromValues( 1, 1, 1 )
      this.diffuseMap  = opts.diffuseMap || null  
      this.specular    = opts.specular || vec3.fromValues( 1, 1, 1 )
      this.specularMap = opts.specularMap || null  
      this.shininess   = opts.shininess || 32
      this.ambient     = this.diffuse

      this.lights = []
      
      this.computeDefs()
      // if ( !this.prg )  this.compileProgram()
      this.compileProgram()
      this.setUniforms()

  }

  computeDefs(  ){

    if ( this.diffuseMap ) {
      this.defs += '#define DIFFUSE_MAP\n'  
    }
    if ( this.specularMap ) {
      this.defs += '#define SPECULAR_MAP\n'  
    }

  }

  compileProgram() {

    this.prg = new Program( this.gl )
    this.prg.compile(
      require('glsl/phong-material.vert'),
      require('glsl/phong-material.frag'),
      this.defs
    )

  }

  setUniforms(){

    var prg = this.prg
    prg.use()
    
    prg[ 'uMaterial.ambient' ]( this.ambient )
    prg[ 'uMaterial.diffuse' ]( this.diffuse )
    prg[ 'uMaterial.specular' ]( this.specular )
    prg[ 'uMaterial.shininess' ]( this.shininess )

    if ( this.diffuseMap ) {
      prg[ 'uMaterial.diffuseMap' ]( this.diffuseMap )
    }
    if ( this.specularMap ) {
      prg[ 'uMaterial.specularMap' ]( this.specularMap )
    }

    prg[ 'uAmbientLight.intensity' ]( .15 )
    prg[ 'uAmbientLight.ambient' ]( [ 1, 1, 1 ] )

  }

  addLight( light ){

    this.lights.push( light )

    this.updateLights()

  }

  updateLights() {

    var prg = this.prg
    
    var light = this.lights[0]

    prg[ 'uLight.intensity' ]( light.intensity )
    prg[ 'uLight.diffuse' ]( light.diffuse )
    prg[ 'uLight.specular' ]( light.specular )
    prg[ 'uLight.position' ]( light.position )

  }

  prepare( node, camera ) { 

    var prg = this.prg
    prg.use()

    this.updateLights()

    mat3.fromMat4( NORMAL_MATRIX, node._matrix )

    if ( this.diffuseMap ) {
      this.diffuseMap.bind()
    }

    if ( this.specularMap ) {
      this.specularMap.bind()
    }

    camera.modelViewProjectionMatrix( M4, node._wmatrix )
    prg.uMVP( M4 )
    prg.uModelMatrix( node._wmatrix )
    prg.uNormalMatrix( NORMAL_MATRIX )
    prg.uCameraPosition( camera.position )

  }

}

export default PhongMaterial