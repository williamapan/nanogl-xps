import Program from 'nanogl/program'
import Node from 'nanogl-node'
import GridGeometry from 'geometries/grid-geometry'
import {mat4} from 'gl-matrix'

var M4   = mat4.create()

var vertexShader = `
  precision highp float;

  attribute vec2 aPosition;

  uniform mat4 uMVP;

  void main(){

    gl_Position = uMVP * vec4( aPosition, 0.0, 1.0 );

  }
`

var fragmentShader = `
  precision highp float;

  void main(){

    float color = 0.3;

    gl_FragColor = vec4( vec3(color), 1.0 );

  }
`

class Grid extends Node {

  constructor( gl ) {
    
    super()

    this.gl = gl

    this.initProgram()

    this.geometry = new GridGeometry( gl )
    this.geometry.attrib( 'aPosition', 2, gl.FLOAT )

    this.drawingMethod = 'drawLines'

  }

  initProgram(){

    this.prg = new Program( this.gl )
    this.prg.compile( vertexShader, fragmentShader )
    this.prg.use()

  }

  bind() {

    this.prg.use()
    this.geometry.bind( this.prg )

  }

  render( camera ){

    this.bind()

    camera.modelViewProjectionMatrix( M4, this._wmatrix )

    this.prg.uMVP( M4 )
  
    this.geometry.render()

  }

}

export default Grid