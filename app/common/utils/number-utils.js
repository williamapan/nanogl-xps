export  {
  limit,
  map,
  hex2rgb,
  degreesToRadians,
  radianToDegrees,
  normalizeRGB,
  mix,
  clamp
}

/**
 function * The clam returns x if it is larger than minVal and smaller than maxVal.
 * In case x is smaller than minVal, minVal is returned.
 * If x is larger than maxVal, maxVal is returned
 *
 * @param {number} number
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
function limit( number, min, max ) {

  return Math.min( Math.max( min, number ), max );

}

/**
 * Re-maps a number from one range to another.
 *
 * @param {number} number
 * @param {number} min1
 * @param {number} max1
 * @param {number} min2
 * @param {number} max2
 * @returns {number} newValue
 */
function map( number, min1, max1, min2, max2 ) {

  var num1 = ( number - min1 ) / ( max1 - min1 );
  var newValue = ( num1 * ( max2 - min2 ) ) + min2;

  return newValue;

}

/**
 * Convert a hex color to rgb
 *
 * @param hex
 * @returns {*[]} rgb
 */
function hex2rgb( hex ) {

  hex = (hex.substr(0,1)=="#") ? hex.substr(1) : hex;
  return [parseInt(hex.substr(0,2), 16), parseInt(hex.substr(2,2), 16), parseInt(hex.substr(4,2), 16)];

}

/**
 * Convert degree to radians
 *
 * @param {number} degree
 * @returns {number} radian
 */
function degreesToRadians( degree ) {

  return degree * ( Math.PI / 180 );

}

/**
 * Convert radian to degree
 *
 * @param {number} radian
 * @returns {number} degree
 */
function radianToDegrees( radian ) {

  return radian * ( 180 / Math.PI );

}

/**
 * Normalize an rgb array
 * [255, 255, 255] -> [ 1, 1, 1 ]
 *
 * @param {Array} color
 * @returns {*[]} rgb
 */
function normalizeRGB( color ) {

  return [ color[0] / 255, color[1] / 255, color[2] / 255 ]

}

function mix( a, b, n ){
  return a*(1.0-n) + b*n;
}

function clamp( n ){
  return Math.max( 0.0, Math.min( 1.0, n ) );
}