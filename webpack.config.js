var path = require("path")

var folder = '5-assimp-model'

module.exports = {

  /**
   * ENTRY FILE(S)
   * Will be overrided by buid/start.js
   */
  entry: `./app/xps/${folder}/index.js`,

  /**
   * ALIAS
   */
  resolve: {
    root: path.resolve(__dirname, 'app'),
    extensions: ['', '.js', '.ts'],
    alias: {
      'camera':     'common/camera',
      'entities':   'common/entities',
      'geometries': 'common/geometries',
      'glsl':       'common/glsl',
      'helpers':    'common/helpers',
      'libs':       'common/libs',
      'lights':     'common/lights',
      'materials':  'common/materials',
      'utils':      'common/utils'
    }
  },
  

  /**
   * OUTPUT
   * path key Will be overrided by buid/start.js
   */
  output: {
    path: path.resolve(__dirname, `public/xps/${folder}/`),
    filename: 'main.js'
  },

  /**
   * LOADERS
   */
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      { 
        test: /\.(glsl|frag|vert)$/, 
        loader: 'raw', 
        exclude: /node_modules/ 
      },
      { 
        test: /\.(glsl|frag|vert)$/, 
        loader: 'glslify-loader', 
        exclude: /node_modules/ 
      }
        
    ]
  }

}